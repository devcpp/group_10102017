package com.kkrasylnykov.l18_filesandpermisionexamples.adapters;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kkrasylnykov.l18_filesandpermisionexamples.fragments.ImageViewFragment;

import java.util.ArrayList;

public class ImageViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> arrData;

    public ImageViewPagerAdapter(FragmentManager fm, ArrayList<String> arrData) {
        super(fm);
        this.arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new ImageViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ImageViewFragment.KEY_PATH, arrData.get(position));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return arrData.size();
    }
}
