package com.kkrasylnykov.l18_filesandpermisionexamples.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.kkrasylnykov.l18_filesandpermisionexamples.R;
import com.kkrasylnykov.l18_filesandpermisionexamples.adapters.ImageViewPagerAdapter;

import java.util.ArrayList;

public class ViewActivity extends AppCompatActivity {
    public static final String KEY_PATHS = "KEY_PATHS";
    public static final String KEY_POSITION = "KEY_POSITION";

    private ViewPager viewPager;
    private ImageViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        Bundle bundle = getIntent().getExtras();
        ArrayList<String> arrData = null;
        int nPositionStart = 0;
        if (bundle != null) {
            arrData = bundle.getStringArrayList(KEY_PATHS);
            nPositionStart = bundle.getInt(KEY_POSITION, 0);
        }

        if (arrData == null) {
            finish();
        }
        viewPager = findViewById(R.id.viewPager);
        adapter = new ImageViewPagerAdapter(getSupportFragmentManager(), arrData);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(nPositionStart);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("devcpp","onPageScrolled -> " + position + " <-> " + positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                Log.d("devcpp","onPageSelected -> " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d("devcpp","onPageScrollStateChanged -> " + state);
            }
        });
    }
}
