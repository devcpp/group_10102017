package com.kkrasylnykov.l18_filesandpermisionexamples.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l18_filesandpermisionexamples.R;

public class ImageViewFragment extends Fragment {

    public static final String KEY_PATH = "KEY_PATHS";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_view, container, false);
        Bundle bundle = getArguments();
        String path = null;
        if (bundle != null){
            path = bundle.getString(KEY_PATH, null);
        }
        ImageView imageView = view.findViewById(R.id.imageViewFragment);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        Display display = ((AppCompatActivity)getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        int nDelta = (options.outWidth / width) + 1;
        options = new BitmapFactory.Options();
        options.inSampleSize = nDelta;

        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        Log.d("devcpp","bitmap -> " + bitmap.getWidth());
        imageView.setImageBitmap(bitmap);
        return view;
    }
}
