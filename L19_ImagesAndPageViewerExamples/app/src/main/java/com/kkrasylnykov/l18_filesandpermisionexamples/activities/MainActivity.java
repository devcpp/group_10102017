package com.kkrasylnykov.l18_filesandpermisionexamples.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l18_filesandpermisionexamples.R;
import com.kkrasylnykov.l18_filesandpermisionexamples.adapters.FilesAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FilesAdapter.OnFileItemClickListener {

    final static int REQUEST_PERMISSION_ON_READ_FILE = 1001;

    private RecyclerView recyclerView;
    private FilesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.filesRecyclerViewMainActivity);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

//        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            readStorage();
        }

    }

    private void readStorage(){
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        Log.d("devcpp", externalStorageDirectory.getAbsolutePath());
        ArrayList<String> files = finedFilesOnDirectory(externalStorageDirectory);
//        for(String imgPath:files){
//            Log.d("devcpp", "imgPath -> " + imgPath);
//        }
        adapter = new FilesAdapter(files);
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_ON_READ_FILE){
            if(ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "NEED READ_EXTERNAL_STORAGE", Toast.LENGTH_LONG).show();
                finish();
            } else {
                readStorage();
            }
        }
    }

    private ArrayList<String> finedFilesOnDirectory(File dir){
        ArrayList<String> results = new ArrayList<>();
        if (dir.exists()) {
            if (dir.isFile()) {
                if (dir.getName().contains(".jpg") || dir.getName().contains(".png") || dir.getName().contains(".jpeg")){
                    results.add(dir.getAbsolutePath());
                }
            } else if (dir.isDirectory()){
                File[] files = dir.listFiles();
                if (files == null) {
                    return results;
                }
                for (File file : files){
                    results.addAll(finedFilesOnDirectory(file));
                }
            }
        }

        return results;
    }

    @Override
    public void onFileItemClick(int position) {
        Intent intent = new Intent(this, ViewActivity.class);
        intent.putExtra(ViewActivity.KEY_PATHS, adapter.getData());
        intent.putExtra(ViewActivity.KEY_POSITION, position);
        startActivity(intent);
    }
}
