package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines;

import android.content.Context;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.PhoneEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers.PhonesDBWrapper;

import java.util.ArrayList;

public class PhoneEngine extends BaseEngine {

    public PhoneEngine(Context context) {
        super(context);
    }

    public ArrayList<PhoneEntity> getPhonesByUserId(long userId){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        return phonesDBWrapper.getPhonesByUserId(userId);
    }

    public void insert(ArrayList<PhoneEntity> phones){
        insert(phones.toArray(new PhoneEntity[]{}));
    }

    public void insert(PhoneEntity... items){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.insert(items);
    }

    public void update(PhoneEntity item){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.update(item);
    }

    public void removePhonesByUserId(long userId){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.removePhonesByUserId(userId);
    }

    public void removePhone(PhoneEntity item){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.removePhone(item);
    }

    public void removeAll(){
        PhonesDBWrapper phonesDBWrapper = new PhonesDBWrapper(getContext());
        phonesDBWrapper.removeAll();
    }
}
