package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.networkWrappers;

import android.content.Context;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class UserNetworkWrapper {
    private static final String BASE_URL = "http://xutpuk.pp.ua";

    private static final String USER_URL = BASE_URL + "/api/users";

    private static final String GET_USER = USER_URL + ".json";
    private static final String INSERT_USER = USER_URL + ".json";
    private static final String REMOVE_USER = USER_URL + "/%d.json";

    public static final int RESPONSE_OK = 200;

    private Context context;

    public UserNetworkWrapper(Context context) {
        this.context = context;
    }

    public ArrayList<UserEntity> getAllUsers(){
        ArrayList<UserEntity> result = new ArrayList<>();
        try {
            HttpURLConnection httpURLConnection = getURLConnection(GET_USER, "GET");

            httpURLConnection.connect();

            int responseCode = httpURLConnection.getResponseCode();
            Log.d("devcpp","responseCode -> " + responseCode);
            if (responseCode == RESPONSE_OK) {
                String strResponse = getResponseString(httpURLConnection);

                if (strResponse != null && !strResponse.isEmpty()) {
                    JSONArray jsonArray = new JSONArray(strResponse);
                    int count = jsonArray.length();
                    for (int i = 0; i < count; i++) {
                        JSONObject item = jsonArray.getJSONObject(i);
                        result.add(new UserEntity(item));
                    }
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("devcpp","MalformedURLException -> " + e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("devcpp","IOException -> " + e.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("devcpp","JSONException -> " + e.toString());
        }
        return result;
    }

    public boolean removeUserById(long nId) {
        boolean result = false;
        try {
            HttpURLConnection httpURLConnection = getURLConnection(String.format(REMOVE_USER, nId),
                    "DELETE");

            httpURLConnection.connect();

            result = httpURLConnection.getResponseCode() == RESPONSE_OK;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void insertUser(UserEntity user){
        try {
            HttpURLConnection httpURLConnection = getURLConnection(INSERT_USER, "POST");

            String strBody = user.toJson();
            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            httpURLConnection.getOutputStream().write(postData);

            httpURLConnection.connect();

            int responseCode = httpURLConnection.getResponseCode();
            Log.d("devcpp","insertUser -> " + responseCode);
            if (responseCode == RESPONSE_OK) {
                String strResponse = getResponseString(httpURLConnection);

                if (strResponse != null && !strResponse.isEmpty()) {
                    JSONObject item = new JSONObject(strResponse);
                    UserEntity userEntity = new UserEntity(item);
                    user.setServerId(userEntity.getServerId());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private HttpURLConnection getURLConnection(String urlString, String method) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod(method);
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setRequestProperty( "Content-Type", "application/json");
        httpURLConnection.setRequestProperty( "charset", "utf-8");
        return httpURLConnection;
    }

    private String getResponseString(HttpURLConnection httpURLConnection) throws IOException {
        InputStream is = httpURLConnection.getInputStream();
        String strResponse = "";
        if (is!=null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            if (reader != null) {
                StringBuilder stringBuilder = new StringBuilder();
                while (true) {
                    String tempStr = reader.readLine();
                    if (tempStr != null) {
                        stringBuilder.append(tempStr);
                    } else {
                        strResponse = stringBuilder.toString();
                        break;
                    }
                }
                reader.close();
            }
            is.close();
        }

        return strResponse;
    }
}