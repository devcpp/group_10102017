package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines;

import android.content.Context;
import android.os.Handler;

public abstract class BaseEngine {
    private Context mContext;
    private Handler handler;

    public BaseEngine(Context context){
        this.mContext = context;
        this.handler = new Handler(context.getMainLooper());
    }

    public Context getContext() {
        return mContext;
    }

    public Handler getHandler() {
        return handler;
    }
}
