package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.PhoneEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers.UserDBWrapper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.networkWrappers.UserNetworkWrapper;

import java.util.ArrayList;

public class UserEngine extends BaseEngine {

    public UserEngine(Context context) {
        super(context);
    }

    public void getAllUsers(final AllUserData callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
                final ArrayList<UserEntity> userEntities = userDBWrapper.getAllUsers();
                PhoneEngine phoneEngine = new PhoneEngine(getContext());
                for(UserEntity item : userEntities){
                    item.setPhones(phoneEngine.getPhonesByUserId(item.getUserId()));
                }

                Log.d("devcpp", Thread.currentThread().getName() + " -> getAllUsers -> " + userEntities.size());
                if (userEntities.size() == 0) {
                    UserNetworkWrapper userNetworkWrapper = new UserNetworkWrapper(getContext());
                    userEntities.addAll(userNetworkWrapper.getAllUsers());
                    if (userEntities.size() != 0) {
                        insertUser(userEntities, new InsertDone() {
                            @Override
                            public void onInsertDone() {
                                getAllUsers(callback);
                            }
                        });
                        return;
                    }
                }
                getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onLoadAllUserData(userEntities);
                    }
                });
//                handler.postDelayed(null, 300);

            }
        }).start();

        //return userEntities;
    }

    public UserEntity getUserById(long nId){
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        UserEntity item = userDBWrapper.getUserById(nId);
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        item.setPhones(phoneEngine.getPhonesByUserId(item.getUserId()));
        return item;
    }

    public void insertUser(final ArrayList<UserEntity> users, final InsertDone callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
                for (UserEntity user : users) {
                    long userId = userDBWrapper.insertUser(user);
                    for (PhoneEntity item : user.getPhones()){
                        item.setUserId(userId);
                    }
                    PhoneEngine phoneEngine = new PhoneEngine(getContext());
                    phoneEngine.insert(user.getPhones());
                }
                getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (callback != null) {
                            callback.onInsertDone();
                        }
                    }
                });
            }
        }).start();
    }

    public void insertUser(final UserEntity user, final InsertDone callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                UserNetworkWrapper userNetworkWrapper = new UserNetworkWrapper(getContext());
                userNetworkWrapper.insertUser(user);

                UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
                long userId = userDBWrapper.insertUser(user);
                for (PhoneEntity item : user.getPhones()){
                    item.setUserId(userId);
                }
                PhoneEngine phoneEngine = new PhoneEngine(getContext());
                phoneEngine.insert(user.getPhones());

                getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (callback != null) {
                            callback.onInsertDone();
                        }
                    }
                });
            }
        }).start();

    }

    public void updateUser(UserEntity user) {
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        userDBWrapper.updateUser(user);
        //TODO Need change update user
    }

    public void removeUser(UserEntity user, RemoveUserDone callback) {
        removeUserById(user.getUserId(), user.getServerId(), callback);
    }

    public void removeUserById(final long nId, final long serverId, final RemoveUserDone callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                PhoneEngine phoneEngine = new PhoneEngine(getContext());
                phoneEngine.removePhonesByUserId(nId);
                UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
                userDBWrapper.removeUser(nId);
                UserNetworkWrapper userNetworkWrapper = new UserNetworkWrapper(getContext());
                userNetworkWrapper.removeUserById(serverId);
                getHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onRemoveUserDone();
                    }
                });
            }
        }).start();
    }

    public void removeAllUsers(){
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        userDBWrapper.removeAllUsers();
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        phoneEngine.removeAll();
    }

    public interface AllUserData {
        void onLoadAllUserData(ArrayList<UserEntity> data);
    }

    public interface InsertDone {
        void onInsertDone();
    }

    public interface RemoveUserDone {
        void onRemoveUserDone();
    }
}
