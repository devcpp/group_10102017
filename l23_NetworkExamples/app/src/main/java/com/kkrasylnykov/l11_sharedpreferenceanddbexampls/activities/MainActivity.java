package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.adapters.UserInfoRecyclerAdapter;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines.UserEngine;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.PhoneEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.networkWrappers.UserNetworkWrapper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        UserInfoRecyclerAdapter.OnClickOnListListener {

    private RecyclerView recyclerView;
    private UserInfoRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        recyclerView = findViewById(R.id.conteinerRecyclerViewMainActivity);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UserInfoRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setOnClickOnListListener(this);

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.addOneButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeOneButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.changeOneButtonMainActivity).setOnClickListener(this);

        if (appSettings.isFistStart()) {
            Log.d("devcpp", "isFistStart -> start");
            appSettings.setIsFistStart(false);
            //TODO NEED ADD CODE PROCESSING FIRST START
            Toast.makeText(this, "FIRST START", Toast.LENGTH_LONG).show();

//            UserEngine userEngine = new UserEngine(this);
//            for (int i=0; i<100; i++){
//                ArrayList<PhoneEntity> phones = new ArrayList<>();
//                phones.add(new PhoneEntity(-1, "095234234" + i));
//                phones.add(new PhoneEntity(-1, "066234234" + i));
//                phones.add(new PhoneEntity(-1, "097234234" + i));
//                UserEntity userEntity = new UserEntity("Name_" + i, "SName_" + i,
//                        phones, "Devichia str, " + i,
//                        (long)(i*100 + i*10 + i));
//                userEngine.insertUser(userEntity);
//            }
//            Thread thread = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    UserNetworkWrapper userNetworkWrapper =
//                            new UserNetworkWrapper(MainActivity.this);
//                    users = userNetworkWrapper.getAllUsers();
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            adapter.updateData(users);
//                        }
//                    });
//                }
//            });
//            thread.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateData();
    }

    private void updateData(){
        Log.d("devcpp", "updateData -> start");
        UserEngine userEngine = new UserEngine(this);
        userEngine.getAllUsers(new UserEngine.AllUserData() {
            @Override
            public void onLoadAllUserData(ArrayList<UserEntity> data) {
                adapter.updateData(data);
            }
        });

        Log.d("devcpp", "updateData -> end");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButtonMainActivity:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAllButtonMainActivity:
                AppDBHelper dbHelper = new AppDBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(DBConstants.DbV1.DB_TABLE_NAME, null, null);
                db.close();

                updateData();
                break;
        }
    }

    @Override
    public void onClickItem(UserEntity item) {
        Intent editActivityIntent = new Intent(this, EditActivity.class);
        editActivityIntent.putExtra(EditActivity.KEY_USER_ID, item.getUserId());
        startActivity(editActivityIntent);
    }

    @Override
    public void onClickButton(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }
}
