package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserEntity extends BaseEntity {

    private long mUserId;
    private long mServerId;
    private String mName;
    private String mSName;
    private ArrayList<PhoneEntity> mPhones;
    private String mAddres;
    private long mBDay;
    private boolean isCollapsed = true;

    public UserEntity(Cursor cursor) {
        int nPositionId = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_ID);
        int nPositionServerId = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_SERVER_ID);
        int nPositionName = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_NAME);
        int nPositionSName = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_SNAME);
        int nPositionAddres = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_ADDRES);
        int nPositionBDay = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_BDAY);

        this.mUserId = cursor.getLong(nPositionId);
        this.mServerId = cursor.getLong(nPositionServerId);
        this.mName = cursor.getString(nPositionName);
        this.mSName = cursor.getString(nPositionSName);
        this.mAddres = cursor.getString(nPositionAddres);
        this.mBDay = cursor.getLong(nPositionBDay);
    }

    public UserEntity(JSONObject object) throws JSONException {
        this.mServerId = object.getLong("id");
        this.mName = object.getString("name");
        this.mSName = object.getString("sname");
        JSONArray address = object.getJSONArray("address");
        if (address.length() > 0) {
            this.mAddres = address.getString(0);
        }
        JSONArray phones = object.getJSONArray("phones");
        this.mPhones = new ArrayList<>();
        for (int i = 0; i < phones.length(); i++) {
            this.mPhones.add(new PhoneEntity(phones.getString(i)));
        }

        this.mBDay = 0;
    }

    public UserEntity(String mName, String mSName, ArrayList<PhoneEntity> phones,
                      String mAddres, long mBDay) {
        this(-1, mName, mSName, phones, mAddres, mBDay);
    }

    public UserEntity(long mUserId, String mName, String mSName, ArrayList<PhoneEntity> phones,
                      String mAddres, long mBDay) {
        this.mUserId = mUserId;
        this.mServerId = -1;
        this.mName = mName;
        this.mSName = mSName;
        this.mPhones = phones;
        this.mAddres = mAddres;
        this.mBDay = mBDay;
    }

    public long getUserId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public long getServerId() {
        return mServerId;
    }

    public void setServerId(long mServerId) {
        this.mServerId = mServerId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public ArrayList<PhoneEntity> getPhones() {
        return mPhones;
    }

    public void setPhones(ArrayList<PhoneEntity> mPhones) {
        this.mPhones = mPhones;
    }

    public String getAddres() {
        return mAddres;
    }

    public void setAddres(String mAddres) {
        this.mAddres = mAddres;
    }

    public long getBDay() {
        return mBDay;
    }

    public void setBDay(long mBDay) {
        this.mBDay = mBDay;
    }

    public boolean isCollapsed() {
        return isCollapsed;
    }

    public void setCollapsed(boolean collapsed) {
        isCollapsed = collapsed;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_SERVER_ID, getServerId());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_NAME, getName());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_SNAME, getSName());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_ADDRES, getAddres());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_BDAY, getBDay());
        return contentValues;
    }

    public String toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", getName());
        jsonObject.put("sname", getSName());
        ArrayList<PhoneEntity> phones = getPhones();
        if (phones!=null && phones.size()>0){
            JSONArray phonesJsonArray = new JSONArray();
            for (PhoneEntity item : phones) {
                phonesJsonArray.put(item.getPhone());
            }

            jsonObject.put("phones", phonesJsonArray);
        }

        String addres = getAddres();
        if (addres!=null && !addres.isEmpty()){
            JSONArray addresJsonArray = new JSONArray();
            addresJsonArray.put(addres);

            jsonObject.put("address", addresJsonArray);
        }
        return jsonObject.toString();
    }
}
