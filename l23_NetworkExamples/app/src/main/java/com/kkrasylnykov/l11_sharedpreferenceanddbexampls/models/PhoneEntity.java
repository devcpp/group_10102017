package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class PhoneEntity extends BaseEntity {
    private long mId;
    private long mUserId;
    private String mPhone;

    public PhoneEntity(Cursor cursor){
        int nPositionId = cursor.getColumnIndex(DBConstants.DbV2.TablePhones.DB_FIELD_ID);
        int nPositionUserId = cursor.getColumnIndex(DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID);
        int nPositionPhone = cursor.getColumnIndex(DBConstants.DbV2.TablePhones.DB_FIELD_PHONE);

        this.mId = cursor.getLong(nPositionId);
        this.mUserId = cursor.getLong(nPositionUserId);
        this.mPhone = cursor.getString(nPositionPhone);
    }

    public PhoneEntity(String mPhone) {
        this(-1, -1, mPhone);
    }

    public PhoneEntity(long mUserId, String mPhone) {
        this(-1, mUserId, mPhone);
    }

    public PhoneEntity(long mId, long mUserId, String mPhone) {
        this.mId = mId;
        this.mUserId = mUserId;
        this.mPhone = mPhone;
    }

    public long getId() {
        return mId;
    }

    public long getUserId() {
        return mUserId;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof PhoneEntity){
            PhoneEntity entity = (PhoneEntity) obj;
            result = entity.getId() == this.getId();
        }
        return result;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID, getUserId());
        contentValues.put(DBConstants.DbV2.TablePhones.DB_FIELD_PHONE, getPhone());
        return contentValues;
    }
}
