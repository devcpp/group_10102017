package l02_functandrecursion;

import java.util.Scanner;

public class L02_FunctAndRecursion {
    
    public static int pow2(int n){
        return n*n*2;
    }
    
    public static float pow2(float n){
        return n*n*2;
    }
    
    /*false
    public static long pow2(int n){
        return n*n*2;
    }*/
    
    public static long pow(int a, int n){
        long result = 0;
        if (n == 0) {
            result = 1;
        } else if (n > 0) {
            result = a * pow(a, n-1);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int a = scanner.nextInt();
        //int rezA = pow2(a);
        
        //int rezB = pow2(6);
        
        int c = scanner.nextInt();
        //int rezC = pow2(c);
        
        //System.out.println("rezA = " + rezA + 
        //        "\nrezB = " + rezB + "\nrezC = " + rezC);
        
        long result = pow(a,c);
        
        System.out.println("result = " + result);
        
    }    
}
