package com.kkrasylnykov.l24_vollyandretrofitexamples.activityes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.GsonRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kkrasylnykov.l24_vollyandretrofitexamples.R;
import com.kkrasylnykov.l24_vollyandretrofitexamples.models.PhoneEntity;
import com.kkrasylnykov.l24_vollyandretrofitexamples.models.UserEntity;
import com.kkrasylnykov.l24_vollyandretrofitexamples.retrofit.UsersAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> phones = new ArrayList<>();
        phones.add("954");
        phones.add("975");
        UserEntity userEntity = new UserEntity(101, 1000, "TestGSON",
                "GSONTest", phones, "Devichia st. 8", 100);

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();

        Gson gson = builder.create();

        final String strData = gson.toJson(userEntity);

        Log.d("devcpp", "strData -> " + strData);

        String strInputData = "{\"sname\":\"GSONTest123\",\"addres\":\"Devichia st. 245\"" +
                ",\"phones\":[\"095\", \"456\", \"369\"]," +
                "\"name\":\"TestGSON123\",\"user_id\":131,\"id\":1005}";

        UserEntity inputUserEntity = gson.fromJson(strInputData, UserEntity.class);
        Log.d("devcpp", "strInputData -> " + inputUserEntity.getName());
        Log.d("devcpp", "strInputData -> " + inputUserEntity.getSName());
        Log.d("devcpp", "strInputData -> " + inputUserEntity.getPhones().size());
        Log.d("devcpp", "strInputData -> " + inputUserEntity.getPhones().get(0));
        Log.d("devcpp", "strInputData -> " + inputUserEntity.getPhones().get(1));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://xutpuk.pp.ua/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final UsersAPI service = retrofit.create(UsersAPI.class);
        service.getUsers().enqueue(new Callback<ArrayList<UserEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<UserEntity>> call, retrofit2.Response<ArrayList<UserEntity>> response) {
                Log.d("devcpp","onResponse -> " + response);
                Log.d("devcpp","users -> " + response.message());
                Log.d("devcpp","users -> " + response.code());
                ArrayList<UserEntity> usersBody = response.body();
                Log.d("devcpp","users -> " + usersBody.size());
                for (UserEntity item : usersBody) {
                    Log.d("devcpp","item -> " + item.getName() + " " + item.getSName());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                Log.d("devcpp","onFailure -> " + t);
            }
        });

        service.createUser(inputUserEntity).enqueue(new Callback<UserEntity>() {
            @Override
            public void onResponse(Call<UserEntity> call, retrofit2.Response<UserEntity> response) {
                Log.d("devcpp","createUser -> onResponse -> " + response);
                UserEntity item = response.body();
                Log.d("devcpp","item -> " + item.getName() + " " + item.getSName());
            }

            @Override
            public void onFailure(Call<UserEntity> call, Throwable t) {
                Log.d("devcpp","createUser -> onFailure -> " + t);
            }
        });

        service.removeUser(108).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
                Log.d("devcpp","removeUser -> onResponse -> " + response);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("devcpp","removeUser -> onFailure -> " + t);
            }
        });

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Call<ArrayList<UserEntity>> users = service.getUsers();
//                try {
//                    retrofit2.Response<ArrayList<UserEntity>> response = users.execute();
//                    Log.d("devcpp","users -> " + response.message());
//                    Log.d("devcpp","users -> " + response.code());
//                    ArrayList<UserEntity> usersBody = response.body();
//                    Log.d("devcpp","users -> " + usersBody.size());
//                    for (UserEntity item : usersBody) {
//                        Log.d("devcpp","item -> " + item.getName() + " " + item.getSName());
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Log.d("devcpp","IOException -> " + e);
//                }
//            }
//        }).start();



//        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
//        String url = "http://xutpuk.pp.ua/api/users.json";
//
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("devcpp", "onResponse -> " + response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("devcpp", "VolleyError -> " + error.toString());
//            }
//        });
//
//        mRequestQueue.add(stringRequest);
//        HashMap<String, String> headers = new HashMap<>();
//        GsonRequest gsonRequest = new GsonRequest(Request.Method.POST, url, UserEntity.class, headers,
//                null, new Response.Listener() {
//            @Override
//            public void onResponse(Object response) {
//                UserEntity userEntity = (UserEntity) response;
//                Log.d("devcpp", "onResponse -> " + userEntity.getName());
//                Log.d("devcpp", "onResponse -> " + userEntity.getPhones().get(0));
//                Log.d("devcpp", "onResponse -> " + userEntity.getPhones().get(1));
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("devcpp", "VolleyError -> " + error.toString());
//            }
//        }) {
//            @Override
//            public byte[] getBody() {
//                Log.d("devcpp", "getBody ");
//                return strData.getBytes();
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=UTF-8";
//            }
//        };
//
//        mRequestQueue.add(gsonRequest);

//        VOLLEY
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = "http://xutpuk.pp.ua/api/users.json";
//        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("devcpp", "onResponse -> " + response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("devcpp", "VolleyError -> " + error.toString());
//            }
//        });
//        queue.add(request);
//
//        StringRequest requestPost = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("devcpp", "requestPost -> onResponse -> " + response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("devcpp", "requestPost -> VolleyError -> " + error.toString());
//            }
//        }){
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                Log.d("devcpp", "getBody ");
//                return strData.getBytes();
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=UTF-8";
//            }
//        } ;
//
//        queue.add(requestPost);

    }
}
