package com.kkrasylnykov.l24_vollyandretrofitexamples.retrofit;

import com.kkrasylnykov.l24_vollyandretrofitexamples.models.UserEntity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UsersAPI {
    @GET("api/users.json")
    Call<ArrayList<UserEntity>> getUsers();

    @POST("api/users.json")
    Call<UserEntity> createUser(@Body UserEntity user);

    @DELETE("/api/users/{userId}.json")
    Call<Void> removeUser(@Path("userId") int id);
}
