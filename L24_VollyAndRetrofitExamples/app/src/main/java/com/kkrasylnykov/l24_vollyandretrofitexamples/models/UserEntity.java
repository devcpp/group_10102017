package com.kkrasylnykov.l24_vollyandretrofitexamples.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserEntity extends BaseEntity {
    @Expose(serialize = true, deserialize = true)
    @SerializedName("user_id")
    private long mUserId;
    @Expose(serialize = true, deserialize = true)
    @SerializedName("id")
    private long mServerId;
    @Expose(serialize = true, deserialize = true)
    @SerializedName("name")
    private String mName;
    @Expose(serialize = true, deserialize = true)
    @SerializedName("sname")
    private String mSName;
    @Expose(serialize = true, deserialize = true)
    @SerializedName("phones")
    private ArrayList<String> mPhones;
    @Expose(serialize = true, deserialize = true)
    @SerializedName("addres")
    private String mAddres;

    @Expose(serialize = false, deserialize = false)
    private long mBDay;
    @Expose(serialize = false, deserialize = false)
    private boolean isCollapsed = true;

//    public UserEntity(JSONObject object) throws JSONException {
//        this.mServerId = object.getLong("id");
//        this.mName = object.getString("name");
//        this.mSName = object.getString("sname");
//        JSONArray address = object.getJSONArray("address");
//        if (address.length() > 0) {
//            this.mAddres = address.getString(0);
//        }
//        JSONArray phones = object.getJSONArray("phones");
//        this.mPhones = new ArrayList<>();
//        for (int i = 0; i < phones.length(); i++) {
//            this.mPhones.add(new PhoneEntity(phones.getString(i)));
//        }
//
//        this.mBDay = 0;
//    }

    public UserEntity(String mName, String mSName, ArrayList<String> phones,
                      String mAddres, long mBDay) {
        this(-1, -1, mName, mSName, phones, mAddres, mBDay);
    }

    public UserEntity(long mUserId, long nServerId, String mName, String mSName, ArrayList<String> phones,
                      String mAddres, long mBDay) {
        this.mUserId = mUserId;
        this.mServerId = nServerId;
        this.mName = mName;
        this.mSName = mSName;
        this.mPhones = phones;
        this.mAddres = mAddres;
        this.mBDay = mBDay;
    }

    public long getUserId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public long getServerId() {
        return mServerId;
    }

    public void setServerId(long mServerId) {
        this.mServerId = mServerId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public ArrayList<String> getPhones() {
        return mPhones;
    }

    public void setPhones(ArrayList<String> mPhones) {
        this.mPhones = mPhones;
    }

    public String getAddres() {
        return mAddres;
    }

    public void setAddres(String mAddres) {
        this.mAddres = mAddres;
    }

    public long getBDay() {
        return mBDay;
    }

    public void setBDay(long mBDay) {
        this.mBDay = mBDay;
    }

    public boolean isCollapsed() {
        return isCollapsed;
    }

    public void setCollapsed(boolean collapsed) {
        isCollapsed = collapsed;
    }

//    public String toJson() throws JSONException {
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("name", getName());
//        jsonObject.put("sname", getSName());
//        ArrayList<PhoneEntity> phones = getPhones();
//        if (phones!=null && phones.size()>0){
//            JSONArray phonesJsonArray = new JSONArray();
//            for (PhoneEntity item : phones) {
//                phonesJsonArray.put(item.getPhone());
//            }
//
//            jsonObject.put("phones", phonesJsonArray);
//        }
//
//        String addres = getAddres();
//        if (addres!=null && !addres.isEmpty()){
//            JSONArray addresJsonArray = new JSONArray();
//            addresJsonArray.put(addres);
//
//            jsonObject.put("address", addresJsonArray);
//        }
//        return jsonObject.toString();
//    }
}
