package com.kkrasylnykov.l24_vollyandretrofitexamples.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.Expose;

public class PhoneEntity extends BaseEntity {

    @Expose
    private long mId;
    @Expose
    private long mUserId;
    @Expose
    private String mPhone;

    public PhoneEntity(String mPhone) {
        this(-1, -1, mPhone);
    }

    public PhoneEntity(long mUserId, String mPhone) {
        this(-1, mUserId, mPhone);
    }

    public PhoneEntity(long mId, long mUserId, String mPhone) {
        this.mId = mId;
        this.mUserId = mUserId;
        this.mPhone = mPhone;
    }

    public long getId() {
        return mId;
    }

    public long getUserId() {
        return mUserId;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof PhoneEntity){
            PhoneEntity entity = (PhoneEntity) obj;
            result = entity.getId() == this.getId();
        }
        return result;
    }
}
