package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class UserEntity {

    private long mUserId;
    private String mName;
    private String mSName;
    private String mPhone;
    private String mAddres;
    private long mBDay;

    public UserEntity(Cursor cursor){
        int nPositionId = cursor.getColumnIndex(DBConstants.DB_FIELD_ID);
        int nPositionName = cursor.getColumnIndex(DBConstants.DB_FIELD_NAME);
        int nPositionSName = cursor.getColumnIndex(DBConstants.DB_FIELD_SNAME);
        int nPositionPhone = cursor.getColumnIndex(DBConstants.DB_FIELD_PHONE);
        int nPositionAddres = cursor.getColumnIndex(DBConstants.DB_FIELD_ADDRES);
        int nPositionBDay = cursor.getColumnIndex(DBConstants.DB_FIELD_BDAY);

        this.mUserId = cursor.getLong(nPositionId);
        this.mName = cursor.getString(nPositionName);
        this.mSName = cursor.getString(nPositionSName);
        this.mPhone = cursor.getString(nPositionPhone);
        this.mAddres = cursor.getString(nPositionAddres);
        this.mBDay = cursor.getLong(nPositionBDay);
    }

    public UserEntity(String mName, String mSName, String mPhone,
                      String mAddres, long mBDay) {
        this(-1, mName, mSName, mPhone, mAddres, mBDay);
    }

    public UserEntity(long mUserId, String mName, String mSName, String mPhone,
                      String mAddres, long mBDay) {
        this.mUserId = mUserId;
        this.mName = mName;
        this.mSName = mSName;
        this.mPhone = mPhone;
        this.mAddres = mAddres;
        this.mBDay = mBDay;
    }

    public long getUserId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getAddres() {
        return mAddres;
    }

    public void setAddres(String mAddres) {
        this.mAddres = mAddres;
    }

    public long getBDay() {
        return mBDay;
    }

    public void setBDay(long mBDay) {
        this.mBDay = mBDay;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstants.DB_FIELD_NAME, getName());
        contentValues.put(DBConstants.DB_FIELD_SNAME, getSName());
        contentValues.put(DBConstants.DB_FIELD_PHONE, getPhone());
        contentValues.put(DBConstants.DB_FIELD_ADDRES, getAddres());
        contentValues.put(DBConstants.DB_FIELD_BDAY, getBDay());
        return contentValues;
    }
}
