package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines.UserEngine;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_USER_ID = "KEY_USER_ID";

    private EditText nameEditText;
    private EditText snameEditText;
    private EditText phoneEditText;
    private EditText addresEditText;

    private long mUserId = -1;
    private long mBDay = (System.currentTimeMillis() / 1000l);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        nameEditText = findViewById(R.id.nameEditTextEditActivity);
        snameEditText = findViewById(R.id.snameEditTextEditActivity);
        phoneEditText = findViewById(R.id.phoneEditTextEditActivity);
        addresEditText = findViewById(R.id.addresEditTextEditActivity);
        //findViewById(R.id.addButtonEditActivity).setOnClickListener(this)

        Button addButton = findViewById(R.id.addButtonEditActivity);
        addButton.setOnClickListener(this);

        Button removeButton = findViewById(R.id.removeButtonEditActivity);
        removeButton.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            mUserId = bundle.getLong(KEY_USER_ID, -1);
        }

        if (mUserId != -1) {
            UserEntity userEntity = new UserEngine(this).getUserById(mUserId);

            mBDay = userEntity.getBDay();

            nameEditText.setText(userEntity.getName());
            snameEditText.setText(userEntity.getSName());
            phoneEditText.setText(userEntity.getPhone());
            addresEditText.setText(userEntity.getAddres());

            addButton.setText("Update");
            removeButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        String strSelection = DBConstants.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(mUserId)};
        switch (v.getId()) {
            case R.id.removeButtonEditActivity:
                new UserEngine(this).removeUserById(mUserId);
                finish();
                break;
            case R.id.addButtonEditActivity:
                String strName = nameEditText.getText().toString();
                String strSName = snameEditText.getText().toString();
                String strPhone = phoneEditText.getText().toString();
                String strAddres = addresEditText.getText().toString();

                if (strName.isEmpty()) {
                    Toast.makeText(this, "Name is emplty!!!!", Toast.LENGTH_LONG).show();
                    return;
                }

                UserEntity userEntity = new UserEntity(mUserId, strName, strSName,
                        strPhone, strAddres, mBDay);

                UserEngine userEngine = new UserEngine(this);

                if (mUserId != -1) {
                    userEngine.updateUser(userEntity);
                    finish();
                    return;
                } else {
                    userEngine.insertUser(userEntity);
                    nameEditText.setText("");
                    snameEditText.setText("");
                    phoneEditText.setText("");
                    addresEditText.setText("");
                    nameEditText.requestFocus();
                }
                break;
        }
    }
}
