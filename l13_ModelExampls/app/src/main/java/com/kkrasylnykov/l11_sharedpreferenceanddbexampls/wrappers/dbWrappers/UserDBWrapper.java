package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserDBWrapper extends BaseDBWrapper {

    public UserDBWrapper(Context context) {
        super(context, DBConstants.DB_TABLE_NAME);
    }

    public ArrayList<UserEntity> getAllUsers(){
        ArrayList<UserEntity> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        if (db != null) {
            Cursor cursor = db.query(getTableName(), null, null,
                    null, null, null, null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    do{
                        result.add(new UserEntity(cursor));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            db.close();
        }
        return result;
    }

    public UserEntity getUserById(long nId){
        UserEntity result = null;
        SQLiteDatabase db = getReadableDB();
        String strSelection = DBConstants.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(getTableName(), null, strSelection,
                argsSelection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()){
                result = new UserEntity(cursor);
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public void insertUser(UserEntity user){
        SQLiteDatabase db = getWritableDB();
        db.insert(getTableName(), null, user.getContentValues());
        db.close();
    }

    public void updateUser(UserEntity user) {
        SQLiteDatabase db = getWritableDB();
        String strSelection = DBConstants.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(user.getUserId())};
        db.update(getTableName(), user.getContentValues(), strSelection, argsSelection);
        db.close();
    }

    public void removeUser(long nId) {
        SQLiteDatabase db = getWritableDB();
        String strSelection = DBConstants.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(nId)};
        db.delete(getTableName(), strSelection, argsSelection);
        db.close();
    }

    public void removeAllUsers(){
        SQLiteDatabase db = getWritableDB();
        db.delete(getTableName(), null, null);
        db.close();
    }
}
