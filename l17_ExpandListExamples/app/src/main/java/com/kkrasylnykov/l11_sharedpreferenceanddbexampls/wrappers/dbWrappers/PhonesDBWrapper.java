package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.PhoneEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class PhonesDBWrapper extends BaseDBWrapper {
    public PhonesDBWrapper(Context context) {
        super(context, DBConstants.DbV2.TablePhones.DB_TABLE_NAME);
    }

    public ArrayList<PhoneEntity> getPhonesByUserId(long userId){
        ArrayList<PhoneEntity> phoneEntities = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(userId)};// "" + userId
        Cursor cursor = db.query(getTableName(), null, strRequest, arrArgs,
                null, null, null);
        if (cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    phoneEntities.add(new PhoneEntity(cursor));
                } while(cursor.moveToNext());

            }
            cursor.close();
        }
        db.close();
        return phoneEntities;
    }

    public void insert(PhoneEntity... items){
        SQLiteDatabase db = getWritableDB();
        for (PhoneEntity item : items) {
            db.insert(getTableName(), null, item.getContentValues());
        }
        db.close();
    }

    public void update(PhoneEntity item){
        if (item.getUserId() == -1) {
            throw new RuntimeException("User Id = -1! Update Error!");
        }
        SQLiteDatabase db = getWritableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getUserId())};
        db.update(getTableName(), item.getContentValues(),
                strRequest, arrArgs);
        db.close();
    }

    public void removePhonesByUserId(long userId){
        if (userId == -1) {
            throw new RuntimeException("User Id = -1! removePhonesByUserId Error!");
        }
        SQLiteDatabase db = getWritableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(userId)};
        db.delete(getTableName(), strRequest, arrArgs);
        db.close();
    }

    public void removePhone(PhoneEntity item){
        if (item.getId() == -1) {
            throw new RuntimeException("Id = -1! removePhone Error!");
        }
        SQLiteDatabase db = getWritableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getId())};
        db.delete(getTableName(), strRequest, arrArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();
        db.delete(getTableName(), null, null);
        db.close();
    }
}
