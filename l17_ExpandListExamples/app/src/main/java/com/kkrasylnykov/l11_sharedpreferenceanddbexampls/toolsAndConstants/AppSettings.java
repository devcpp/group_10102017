package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {
    private final static String KEY_BOOLEAN_IS_FIST_START = "KEY_BOOLEAN_IS_FIST_START";

    private SharedPreferences mSharedPreferences;

    public AppSettings(Context context){
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isFistStart(){
        return mSharedPreferences.getBoolean(KEY_BOOLEAN_IS_FIST_START, true);
    }

    public void setIsFistStart(boolean isFistStart){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_FIST_START, isFistStart);
        editor.commit();
    }
}
