package l02_arrayexample;

import java.util.Scanner;

public class L02_ArrayExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        String str1 = scanner.nextLine();
        
        System.out.println("str1 = " + str1);
        
        System.out.print("Enter count: ");
        int count = scanner.nextInt();
        
        float[] arrSizes = new float[count];
        
        for (int i = 0; i < arrSizes.length; i++){
            do{
                System.out.print("Enter " + (i + 1) + ": ");
                arrSizes[i] = scanner.nextFloat();
            } while(arrSizes[i] <= 0);
            
        }
        
        float sum = 0;
        
        for (float size : arrSizes){
            sum += size;
        }
        
        System.out.println("sum = " + sum);
        
        scanner.nextLine();
    }
    
}
