package com.kkrasylnykov.l32_reflexexample;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Field[] fields = Activity.class.getFields();
        for (Field field : fields) {
            Log.d("devcppRef", "field -> " + field.getName());
        }

        Method[] methods = Activity.class.getDeclaredMethods();
        for (Method method : methods) {
            Log.d("devcppRef", "method -> " + method.getName());
        }

        try {
            A a = new A();
            Method method = A.class.getDeclaredMethod("privateMethode");
            method.setAccessible(true);
            method.invoke(a);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    private class A{

        private void privateMethode(){
            Log.d("devcppRef","privateMethode -> run!!!!!");
        }
    }
}
