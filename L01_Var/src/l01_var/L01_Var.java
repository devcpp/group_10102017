package l01_var;

import java.util.Scanner;

public class L01_Var {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int nMyVar = 1;
        float fMyNum = (float)0.1;
        float fMyNum2 = 0.1f;
        
        int nInt = 100;
        long nLog = nInt;
        
        nInt = (int) nLog;
        
        int nVar1 = scanner.nextInt();
        int nVar2 = scanner.nextInt();
        
        //float fResult = ((float) nVar1)/nVar2;
        
//       System.out.println("result = " + fResult);
        int nResult = 0;

        if ((nVar2 != 0) && (nVar1 / nVar2 == 5)) {
            nResult = nVar1 - nVar2;
        } else if (nVar1 == nVar2) {
            nResult = -1;
        } else {
            nResult = nVar1 * nVar2;
        }
        
        System.out.println("result = " + nResult);
        
    }
    
}
