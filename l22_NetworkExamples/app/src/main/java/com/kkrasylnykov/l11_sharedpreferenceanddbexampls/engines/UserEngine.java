package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines;

import android.content.Context;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.PhoneEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers.UserDBWrapper;

import java.util.ArrayList;

public class UserEngine extends BaseEngine {

    public UserEngine(Context context) {
        super(context);
    }

    public ArrayList<UserEntity> getAllUsers(){
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        ArrayList<UserEntity> userEntities = userDBWrapper.getAllUsers();
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        for(UserEntity item : userEntities){
            item.setPhones(phoneEngine.getPhonesByUserId(item.getUserId()));
        }
        return userEntities;
    }

    public UserEntity getUserById(long nId){
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        UserEntity item = userDBWrapper.getUserById(nId);
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        item.setPhones(phoneEngine.getPhonesByUserId(item.getUserId()));
        return item;
    }

    public void insertUser(UserEntity user){
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        long userId = userDBWrapper.insertUser(user);
        for (PhoneEntity item : user.getPhones()){
            item.setUserId(userId);
        }
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        phoneEngine.insert(user.getPhones());
    }

    public void updateUser(UserEntity user) {
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        userDBWrapper.updateUser(user);
        //TODO Need change update user
    }

    public void removeUser(UserEntity user) {
        removeUserById(user.getUserId());
    }

    public void removeUserById(long nId) {
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        phoneEngine.removePhonesByUserId(nId);
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        userDBWrapper.removeUser(nId);
    }

    public void removeAllUsers(){
        UserDBWrapper userDBWrapper = new UserDBWrapper(getContext());
        userDBWrapper.removeAllUsers();
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        phoneEngine.removeAll();
    }
}
