package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines;

import android.content.Context;

public abstract class BaseEngine {
    private Context mContext;

    public BaseEngine(Context context){
        this.mContext = context;
    }

    public Context getContext() {
        return mContext;
    }
}
