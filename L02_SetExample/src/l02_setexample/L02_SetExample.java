package l02_setexample;

import java.util.HashSet;

public class L02_SetExample {

    public static void main(String[] args) {
        HashSet<Integer> setData = new HashSet<>();
        
        boolean isAdd = setData.add(34);
        
        boolean isRemove = setData.remove(65);
        
        System.out.println("isAdd -> " + isAdd);
        System.out.println("isRemove -> " + isRemove);
        
        isAdd = setData.add(4);
        
        System.out.println("isAdd2 -> " + isAdd);
        
        boolean isContains = setData.contains(34);
        
        setData.add(78);
        
        System.out.println("isContains -> " + isContains);
        
        isContains = setData.contains(78);
        
        System.out.println("isContains2 -> " + isContains);
        
        for (Integer item : setData){
            System.out.println(item + " ");
        }
    }
    
}
