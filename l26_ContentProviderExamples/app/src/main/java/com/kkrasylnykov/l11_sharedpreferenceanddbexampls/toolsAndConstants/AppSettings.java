package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.NotepadApplication;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.providers.AppSettingsContentProvider;

public class AppSettings {

    private static AppSettings sInstance;

//    public static AppSettings getInstance(Context context){
//        if (sInstance == null) {
//            sInstance = new AppSettings(context);
//        }
//        return sInstance;
//    }

//    public static AppSettings getInstance(Context context) {
//        AppSettings localInstance = sInstance;
//        if (localInstance == null) {
//            synchronized (AppSettings.class) {
//                localInstance = sInstance;
//                if (localInstance == null) {
//                    sInstance = localInstance = new AppSettings(context);
//                }
//            }
//        }
//        return localInstance;
//    }

    public static AppSettings getInstance(){
        AppSettings localInstance = sInstance;
        if (localInstance == null) {
            synchronized (AppSettings.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new AppSettings();
                }
            }
        }
        return localInstance;
    }


    private AppSettings(){
    }

    public boolean isFistStart(){
        Cursor cursor = NotepadApplication.getAppContext().
                getContentResolver().query(AppSettingsContentProvider.IS_FIRST_START_URI,
                null, null, null, null);
        boolean isFirst = false;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                isFirst = cursor.getInt(0) != 0;
            }
            cursor.close();
        }
        return isFirst;
    }

    public void setIsFistStart(boolean isFistStart){
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppSettingsContentProvider.DATA, isFistStart);
        NotepadApplication.getAppContext().
                getContentResolver().insert(AppSettingsContentProvider.IS_FIRST_START_URI, contentValues);
}

    public int getCountStart(){
        Cursor cursor = NotepadApplication.getAppContext().
                getContentResolver().query(AppSettingsContentProvider.COUNT_START_URI,
                null, null, null, null);
        int nCount = 0;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                nCount = cursor.getInt(0);
            }
            cursor.close();
        }
        return nCount;
    }

    public void incStartApp(){
        int nCount = getCountStart() + 1;
        ContentValues contentValues = new ContentValues();
        contentValues.put(AppSettingsContentProvider.DATA, nCount);
        Log.d("devcpp","incStartApp -> nCount -> " + nCount);
        NotepadApplication.getAppContext().
                getContentResolver().insert(AppSettingsContentProvider.COUNT_START_URI, contentValues);
    }
}
