package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;


public class StartOnLoadService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Log.d("devcpp", "StartOnLoadService -> onStartCommand -> "
                            + AppSettings.getInstance().getCountStart());
                    Log.d("devcpp", "StartOnLoadService -> onStartCommand -> "
                            + AppSettings.getInstance());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
