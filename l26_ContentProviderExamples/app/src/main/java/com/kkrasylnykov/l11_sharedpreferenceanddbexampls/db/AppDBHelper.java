package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class AppDBHelper extends SQLiteOpenHelper {

    public AppDBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDbV2(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("devcpp", "onUpgrade -> oldVersion -> " + oldVersion);
        Log.d("devcpp", "onUpgrade -> newVersion -> " + newVersion);
        if (oldVersion < 2) {
            Log.d("devcpp", "onUpgrade -> start");
            int nCount = 0;
            createDbV2(db);

            Cursor cursorOldData = db.query(DBConstants.DbV1.DB_TABLE_NAME, null,
                    null, null, null, null, null);
            Log.d("devcpp", "onUpgrade -> cursorOldData -> " + cursorOldData);
            if (cursorOldData != null) {
                if (cursorOldData.moveToFirst()){
                    Log.d("devcpp", "onUpgrade -> cursorOldData -> moveToFirst");
                    int nPositionName = cursorOldData.getColumnIndex(DBConstants.DbV1.DB_FIELD_NAME);
                    int nPositionSName = cursorOldData.getColumnIndex(DBConstants.DbV1.DB_FIELD_SNAME);
                    int nPositionPhone = cursorOldData.getColumnIndex(DBConstants.DbV1.DB_FIELD_PHONE);
                    int nPositionAddres = cursorOldData.getColumnIndex(DBConstants.DbV1.DB_FIELD_ADDRES);
                    int nPositionBDay = cursorOldData.getColumnIndex(DBConstants.DbV1.DB_FIELD_BDAY);
                    db.beginTransaction();
                    do {
                        nCount++;
                        String name = cursorOldData.getString(nPositionName);
                        String sname = cursorOldData.getString(nPositionSName);
                        String phone = cursorOldData.getString(nPositionPhone);
                        String addres = cursorOldData.getString(nPositionAddres);
                        long bday = cursorOldData.getLong(nPositionBDay);

                        ContentValues userContentValues = new ContentValues();
                        userContentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_NAME, name);
                        userContentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_SNAME, sname);
                        userContentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_ADDRES, addres);
                        userContentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_BDAY, bday);

                        long userId = db.insert(DBConstants.DbV2.TableUsers.DB_TABLE_NAME,
                                null, userContentValues);

                        ContentValues phoneContentValues = new ContentValues();
                        phoneContentValues.put(DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID, userId);
                        phoneContentValues.put(DBConstants.DbV2.TablePhones.DB_FIELD_PHONE, phone);

                        db.insert(DBConstants.DbV2.TablePhones.DB_TABLE_NAME,
                                null, phoneContentValues);

                    } while (cursorOldData.moveToNext());
                    db.setTransactionSuccessful();
                    db.endTransaction();
                }
                cursorOldData.close();
            }

            db.execSQL("DROP TABLE " + DBConstants.DbV1.DB_TABLE_NAME);
            Log.d("devcpp", "onUpgrade -> end -> " + nCount);
        }


//        if (oldVersion < 3){
//
//        }
//        if (oldVersion < 4){
//
//        }
    }

    private void createDbV2(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.DbV2.TableUsers.DB_TABLE_NAME +
                " ( " + DBConstants.DbV2.TableUsers.DB_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.DbV2.TableUsers.DB_FIELD_NAME + " TEXT NOT NULL, " +
                DBConstants.DbV2.TableUsers.DB_FIELD_SNAME + " TEXT, " +
                DBConstants.DbV2.TableUsers.DB_FIELD_ADDRES + " TEXT, " +
                DBConstants.DbV2.TableUsers.DB_FIELD_BDAY + " INTEGER);");

        db.execSQL("CREATE TABLE " + DBConstants.DbV2.TablePhones.DB_TABLE_NAME +
                " ( " + DBConstants.DbV2.TablePhones.DB_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID + " INTEGER NOT NULL, " +
                DBConstants.DbV2.TablePhones.DB_FIELD_PHONE + " TEXT NOT NULL);");
    }
}
