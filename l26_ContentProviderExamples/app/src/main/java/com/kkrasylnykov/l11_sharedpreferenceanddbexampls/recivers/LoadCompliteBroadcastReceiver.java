package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.services.StartOnLoadService;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;

public class LoadCompliteBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("devcpp", "LoadCompliteBroadcastReceiver -> onReceive -> "
                + AppSettings.getInstance().getCountStart());
        Log.d("devcpp", "LoadCompliteBroadcastReceiver -> onReceive -> "
                + AppSettings.getInstance());

        context.startService(new Intent(context, StartOnLoadService.class));
    }
}
