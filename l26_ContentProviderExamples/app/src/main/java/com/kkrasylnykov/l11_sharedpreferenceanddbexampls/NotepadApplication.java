package com.kkrasylnykov.l11_sharedpreferenceanddbexampls;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;

public class NotepadApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("devcpp","NotepadApplication -> onCreate");

        context = this;

//        AppSettings.init(this);
    }

    public static Context getAppContext(){
        return context;
    }

}


