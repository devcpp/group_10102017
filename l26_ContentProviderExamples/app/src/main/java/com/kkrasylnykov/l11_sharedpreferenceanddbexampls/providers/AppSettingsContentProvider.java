package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class AppSettingsContentProvider extends ContentProvider {
    private final static String KEY_BOOLEAN_IS_FIST_START = "KEY_BOOLEAN_IS_FIST_START";
    private final static String KEY_INT_COUNT_START = "KEY_INT_COUNT_START";

    private static final String AUTTHORITIES = "com.kkrasylnykov.l11_sharedpreferenceanddbexampls.providers.AppSettingsContentProvider";

    private static final String IS_FIRST_START = "is_first_start";
    private static final String COUNT_START = "count_start";

    public static final Uri IS_FIRST_START_URI = Uri.parse("content://" + AUTTHORITIES + "/" + IS_FIRST_START);
    public static final Uri COUNT_START_URI = Uri.parse("content://" + AUTTHORITIES + "/" + COUNT_START);

    public static final String DATA = "DATA";

    private static final int CODE_IS_FIRST_START = 1001;
    private static final int CODE_COUNT_START = 1002;

    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTTHORITIES, IS_FIRST_START, CODE_IS_FIRST_START);
        uriMatcher.addURI(AUTTHORITIES, COUNT_START, CODE_COUNT_START);
    }

    private SharedPreferences mSharedPreferences;

    @Override
    public boolean onCreate() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        Object data = null;
        switch (uriMatcher.match(uri)){
            case CODE_COUNT_START:
                data = mSharedPreferences.getInt(KEY_INT_COUNT_START, 0);
                break;
            case CODE_IS_FIRST_START:
                data = mSharedPreferences.getBoolean(KEY_BOOLEAN_IS_FIST_START, true) ? 1 : 0;
                break;
        }

        MatrixCursor cursor = new MatrixCursor(new String[]{DATA}, 1);
        cursor.addRow(new Object[]{data});
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        switch (uriMatcher.match(uri)){
            case CODE_COUNT_START:
                int nCount = values.getAsInteger(DATA);
                editor.putInt(KEY_INT_COUNT_START, nCount);
                break;
            case CODE_IS_FIRST_START:
                boolean isFistStart = values.getAsBoolean(DATA);
                editor.putBoolean(KEY_BOOLEAN_IS_FIST_START, isFistStart);
                break;
        }
        editor.commit();
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }
}
