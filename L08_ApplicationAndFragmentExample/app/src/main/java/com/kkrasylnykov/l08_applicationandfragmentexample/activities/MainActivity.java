package com.kkrasylnykov.l08_applicationandfragmentexample.activities;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kkrasylnykov.l08_applicationandfragmentexample.fragments.ListFragment;
import com.kkrasylnykov.l08_applicationandfragmentexample.R;
import com.kkrasylnykov.l08_applicationandfragmentexample.fragments.ViewFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ListFragment mListFragment;
    ViewFragment mViewFragment;
//    ListFragment mListFragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("devcpp","MainActivity -> onCreate");

        mListFragment = new ListFragment();
        mViewFragment = new ViewFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frameLayout, mListFragment);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            fragmentTransaction.add(R.id.viewFrameLayout, mViewFragment);
        }

        fragmentTransaction.commit();
//        mListFragment2 = new ListFragment();

//        findViewById(R.id.addFragmentButton).setOnClickListener(this);
//        findViewById(R.id.addFragment2Button).setOnClickListener(this);
//        findViewById(R.id.removeFragmentButton).setOnClickListener(this);
    }

    public void setInfo(String strInfo){
        Log.d("devcpp", "setInfo -> " + strInfo);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.add(R.id.frameLayout, mViewFragment);
            fragmentTransaction.commit();
        }

        mViewFragment.setInfo(strInfo);
    }

    @Override
    public void onClick(View v) {
//        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//        switch (v.getId()){
//            case R.id.addFragmentButton:
//                fragmentTransaction.replace(R.id.frameLayout, mListFragment);
//                break;
//            case R.id.addFragment2Button:
//                fragmentTransaction.replace(R.id.frameLayout2, mListFragment);
//                break;
//            case R.id.removeFragmentButton:
//                fragmentTransaction.remove(mListFragment);
//                break;
//        }
//        fragmentTransaction.commit();
    }
}
