package com.kkrasylnykov.l08_applicationandfragmentexample.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l08_applicationandfragmentexample.R;

public class ViewFragment extends Fragment {

    private View mRootView;
    private TextView mInfoTextView;
    private String mInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_view, container, false);
        mInfoTextView = mRootView.findViewById(R.id.infoTextViewViewFragment);
        if (mInfo!=null && !mInfo.isEmpty()){
            mInfoTextView.setText(mInfo);
        }
        return mRootView;
    }

    public void setInfo(String info){
        mInfo = info;
        if (mInfoTextView != null) {
            mInfoTextView.setText(mInfo);
        }

    }
}
