package com.kkrasylnykov.l08_applicationandfragmentexample.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kkrasylnykov.l08_applicationandfragmentexample.R;
import com.kkrasylnykov.l08_applicationandfragmentexample.activities.MainActivity;

public class ListFragment extends Fragment implements View.OnClickListener {

    View mRootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_list, container, false);
        mRootView.findViewById(R.id.button1ListFragmet).setOnClickListener(this);
        mRootView.findViewById(R.id.button2ListFragmet).setOnClickListener(this);
        mRootView.findViewById(R.id.button3ListFragmet).setOnClickListener(this);
        mRootView.findViewById(R.id.button4ListFragmet).setOnClickListener(this);
        mRootView.findViewById(R.id.button5ListFragmet).setOnClickListener(this);

        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("devcpp", "ListFragment -> onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("devcpp", "ListFragment -> onDetach");
    }

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button button = (Button) v;
            String strName = button.getText().toString();

            Activity activity = getActivity();
            if (isAdded() && activity instanceof MainActivity) {
                MainActivity mainActivity = (MainActivity) activity;
                mainActivity.setInfo(strName);
            }
        }
    }
}
