package com.kkrasylnykov.l08_applicationandfragmentexample;

import android.app.Application;
import android.util.Log;

public class AppExample extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp","AppExample -> onCreate");
    }
}
