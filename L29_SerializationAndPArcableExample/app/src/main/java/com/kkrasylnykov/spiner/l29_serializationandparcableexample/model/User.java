package com.kkrasylnykov.spiner.l29_serializationandparcableexample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class User implements Parcelable {
    private long id;
    private String token;
    private String name;
    private String displayName;
    private String tel;
    private String email;
    private boolean isAdmin;
    private float discount;
    private ArrayList<String> addresses;

    public User(long id, String token, String name, String displayName, String tel, String email, boolean isAdmin, float discount, ArrayList<String> addresses) {
        this.id = id;
        this.token = token;
        this.name = name;
        this.displayName = displayName;
        this.tel = tel;
        this.email = email;
        this.isAdmin = isAdmin;
        this.discount = discount;
        this.addresses = addresses;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.token);
        dest.writeString(this.name);
        dest.writeString(this.displayName);
        dest.writeString(this.tel);
        dest.writeString(this.email);
        dest.writeByte(this.isAdmin ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.discount);
        dest.writeStringList(this.addresses);
    }

    protected User(Parcel in) {
        this.id = in.readLong();
        this.token = in.readString();
        this.name = in.readString();
        this.displayName = in.readString();
        this.tel = in.readString();
        this.email = in.readString();
        this.isAdmin = in.readByte() != 0;
        this.discount = in.readFloat();
        this.addresses = in.createStringArrayList();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
