package com.kkrasylnykov.spiner.l29_serializationandparcableexample.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.kkrasylnykov.spiner.l29_serializationandparcableexample.model.User;

public class AppData {

    public static final String KEY_USER = "KEY_USER";

    private SharedPreferences mSharedPreferences;
    private Gson gson;

    public AppData(Context context){
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        gson = new Gson();
    }

    public void setUser(@Nullable User user){
        Editor editor = mSharedPreferences.edit();
        if (user == null) {
            editor.remove(KEY_USER);
        } else {
            String json = gson.toJson(user);
            editor.putString(KEY_USER, json) ;

            Log.d("devcpp", json);
        }
        editor.commit();

    }

    public User getUser(){
        String data = mSharedPreferences.getString(KEY_USER, null);
        User user = null;
        if (data != null) {
            user = gson.fromJson(data, User.class);
        }
        return user;
    }
}
