package com.kkrasylnykov.spiner.l29_serializationandparcableexample.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.kkrasylnykov.spiner.l29_serializationandparcableexample.R;
import com.kkrasylnykov.spiner.l29_serializationandparcableexample.model.User;
import com.kkrasylnykov.spiner.l29_serializationandparcableexample.tools.AppData;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String DATA = "DATA";

    public static Intent getNewInsnatce(Context context){
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.addUser).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        EditText idEditText = findViewById(R.id.userid);
        EditText tokenEditText = findViewById(R.id.userToken);
        EditText nameEditText = findViewById(R.id.userName);
        EditText diplayNameEditText = findViewById(R.id.userDiplayName);
        EditText phoneEditText = findViewById(R.id.userPhone);
        EditText emailEditText = findViewById(R.id.userEmail);
        EditText discontEditText = findViewById(R.id.userDiscont);
        CheckBox isAdminCheckBox = findViewById(R.id.isAdminCheckBox);


        long id = Long.parseLong(idEditText.getText().toString());
        String token = tokenEditText.getText().toString();
        String name = nameEditText.getText().toString();
        String diplayName = diplayNameEditText.getText().toString();
        String phone = phoneEditText.getText().toString();
        String email = emailEditText.getText().toString();
        float discont = Float.parseFloat(discontEditText.getText().toString());
        boolean isAdmin = isAdminCheckBox.isChecked();
        String addr1 = "addr1";
        String addr2 = "addr2";
        String addr3 = "addr3";
        String addr4 = "addr4";
        ArrayList<String> addresses = new ArrayList<>();
        addresses.add(addr1);
        addresses.add(addr2);
        addresses.add(addr3);
        addresses.add(addr4);

        User user = new User(id, token, name, diplayName, phone, email, isAdmin, discont, addresses);

        AppData appData = new AppData(this);
        appData.setUser(user);

        Intent intent = new Intent();
        intent.putExtra(DATA, user);

        setResult(RESULT_OK, intent);
        finish();
    }
}
