package com.kkrasylnykov.spiner.l29_serializationandparcableexample.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.kkrasylnykov.spiner.l29_serializationandparcableexample.R;
import com.kkrasylnykov.spiner.l29_serializationandparcableexample.model.User;
import com.kkrasylnykov.spiner.l29_serializationandparcableexample.tools.AppData;

import static com.kkrasylnykov.spiner.l29_serializationandparcableexample.activities.LoginActivity.getNewInsnatce;

public class MainActivity extends AppCompatActivity {

    private static final int REQUESR_CODE_LOGIN = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppData appData = new AppData(this);
        User user = appData.getUser();

        if (user == null) {
            startActivityForResult(getNewInsnatce(this), REQUESR_CODE_LOGIN);
        } else {
            Log.d("devcpp","onCreate -> user -> " + user.getName());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUESR_CODE_LOGIN) {
            if (resultCode == RESULT_OK) {

                if (data != null) {

                    User user = data.getExtras().getParcelable(LoginActivity.DATA);
                    Log.d("devcpp","user -> " + user.getName());
                }

            } else {
                finish();
            }
        }
    }
}
