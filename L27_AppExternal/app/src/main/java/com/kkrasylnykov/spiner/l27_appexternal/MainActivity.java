package com.kkrasylnykov.spiner.l27_appexternal;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageView;

    private static final String AUTHORITIES
            = "com.kkrasylnykov.l11_sharedpreferenceanddbexampls.providers.DbExternalProvider3817845639";

    private static final String PATH_USERS = "users";
    private static final String PATH_PHONES = "phones";

    private Button download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.ImageView);
        download = findViewById(R.id.ButtonDownload);
        download.setOnClickListener(this);
        findViewById(R.id.ButtonShare).setOnClickListener(this);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        Uri imageUri = null;
        if (bundle!=null) {
            imageUri = bundle.getParcelable(Intent.EXTRA_STREAM);
        }

        if (imageUri!=null) {
            download.setEnabled(false);
            imageView.setImageURI(imageUri);
        } else if (intent.getData() != null) {
            downloadData(intent.getData().toString());
        }

        Uri uri = Uri.parse("content://" + AUTHORITIES + "/" + PATH_USERS);
        Cursor cur = getContentResolver().query(uri, null, "_sname LIKE ?",
                new String[]{"%8%"}, null);

        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    Log.d("devcpp", cur.getString(1) + " "
                            + cur.getString(2) + ": " + cur.getString(3));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {

        }

        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    Log.d("devcpp", contactId + " -> " + displayName + ":");
                    if (Boolean.parseBoolean(hasPhone)) {
                        // You know it has a number so now query it like this
                        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{contactId}, null);
                        if (phones != null) {
                            if (phones.moveToFirst()) {
                                do {
                                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    Log.d("devcpp", "phoneNumber -> " + phoneNumber);
                                } while (phones.moveToNext());
                                phones.close();
                            }

                        }

                    }

                    Cursor emails = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);

                    if (emails != null) {
                        if (emails.moveToFirst()) {
                            do {
                                String emailAddress = emails.getString(
                                        emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                Log.d("devcpp", "emailAddress -> " + emailAddress);
                            } while (emails.moveToNext());

                            emails.close();
                        }
                    }
                } while ((cursor.moveToNext()));
            }


            cursor.close();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ButtonDownload:
                downloadData();
                break;
            case R.id.ButtonShare:
                share();
                break;
        }
    }

    private void downloadData() {
        downloadData("https://blogs-images.forbes.com/kevinmurnane/files/2017/10/LG-4K-TV_LG.jpg");
    }

    private void downloadData(String strUrl) {
        Picasso.with(this)
                .load(strUrl)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d("devcpp", "onSuccess -> ");
                        download.setEnabled(false);
                    }

                    @Override
                    public void onError() {
                        Log.d("devcpp", "onError -> ");
                    }
                });
    }

    private void share() {
        Drawable drawable = imageView.getDrawable();
        if (drawable == null) {
            Toast.makeText(this, "drawable == null", Toast.LENGTH_LONG).show();
            return;
        }

        File fileDir = new File(getFilesDir(), "image");
        if (!fileDir.exists()) {
            fileDir.mkdir();
        }
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

        Bitmap watermark = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_android_studio);

        Bitmap resultBitmap = Bitmap.createBitmap(bitmap.getWidth() + 20,
                bitmap.getHeight() + 20, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(resultBitmap);

        int nXPosition = bitmap.getWidth() - watermark.getWidth() - 20;
        int nYPosition = bitmap.getHeight() - watermark.getHeight() - 20;

        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        Paint paint2 = new Paint();
        paint2.setColor(Color.RED);
        paint2.setStyle(Paint.Style.FILL);

        canvas.drawPaint(paint2);

        canvas.drawBitmap(bitmap, 10, 10, mPaint);
        canvas.drawBitmap(watermark, nXPosition, nYPosition, mPaint);

        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(fileDir, fileName);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        resultBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);

        byte[] bitmapData = bos.toByteArray();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri uri = FileProvider.getUriForFile(this,
                "com.kkrasylnykov.spiner.l27_appexternal", file);


        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(shareIntent);
    }
}
