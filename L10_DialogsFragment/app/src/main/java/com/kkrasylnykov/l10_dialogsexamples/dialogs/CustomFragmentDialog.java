package com.kkrasylnykov.l10_dialogsexamples.dialogs;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kkrasylnykov.l10_dialogsexamples.R;

public class CustomFragmentDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_custom, container, false);
        return rootView;
    }

    public void show(FragmentManager manager) {
        super.show(manager, getFragmentTag());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("devcpp","CustomFragmentDialog -> onSaveInstanceState");
    }

    public static String getFragmentTag(){
        return "CustomFragmentDialog";
    }
}
