package com.kkrasylnykov.l10_dialogsexamples.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.kkrasylnykov.l10_dialogsexamples.R;
import com.kkrasylnykov.l10_dialogsexamples.dialogs.CustomDialog;
import com.kkrasylnykov.l10_dialogsexamples.dialogs.CustomFragmentDialog;

import static android.app.TimePickerDialog.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String KEY_SAVE_COUNT = "KEY_SAVE_COUNT";

    private int nCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState!=null){
            nCount = savedInstanceState.getInt(KEY_SAVE_COUNT, 0);

            Log.d("devcpp", "nCount -> " + nCount);
        }

        findViewById(R.id.alertDialogButton).setOnClickListener(this);
        findViewById(R.id.progressDialogButton).setOnClickListener(this);
        findViewById(R.id.timeDialogButton).setOnClickListener(this);
        findViewById(R.id.dateDialogButton).setOnClickListener(this);
        findViewById(R.id.customDialogButton).setOnClickListener(this);
        findViewById(R.id.fragmentDialogButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        nCount++;
        switch (v.getId()){
            case R.id.alertDialogButton:
                AlertDialog.Builder allertBuilder = new AlertDialog.Builder(MainActivity.this);
                allertBuilder.setTitle(R.string.app_name);
                allertBuilder.setMessage("Dialog message");
                allertBuilder.setPositiveButton("Pos", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO NEED ADD PROCESSING
                        Toast.makeText(MainActivity.this, "onClick -> Positive", Toast.LENGTH_LONG).show();
                    }
                });

                allertBuilder.setNegativeButton("Neg", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO NEED ADD PROCESSING
                        Toast.makeText(MainActivity.this, "onClick -> Negative", Toast.LENGTH_LONG).show();
                    }
                });

                allertBuilder.setNeutralButton("Neut", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO NEED ADD PROCESSING
                        Toast.makeText(MainActivity.this, "onClick -> Neutral", Toast.LENGTH_LONG).show();
                    }
                });

                allertBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Toast.makeText(MainActivity.this, "onCancel!!!!!!!!!!!!!", Toast.LENGTH_LONG).show();
                    }
                });

//                allertBuilder.setCancelable(false);

                AlertDialog alertDialog = allertBuilder.create();
                alertDialog.show();

//                alertDialog.dismiss();
//                AlertDialog alertDialog = new AlertDialog(MainActivity.this);
                break;
            case R.id.progressDialogButton:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Please wait...");
                progressDialog.show();
                break;
            case R.id.timeDialogButton:
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                Toast.makeText(MainActivity.this,
                                        "hourOfDay -> " + hourOfDay +
                                                "; minute -> " + minute, Toast.LENGTH_LONG).show();
                            }
                        }, 0, 0, true);
                timePickerDialog.show();
                break;
            case R.id.dateDialogButton:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Toast.makeText(MainActivity.this,
                                        "year -> " + year +
                                                "; month -> " + month +
                                                "; dayOfMonth -> " + dayOfMonth,
                                        Toast.LENGTH_LONG).show();
                            }
                        }, 2017, 11, 9);
                datePickerDialog.show();
                break;
            case R.id.customDialogButton:
                CustomDialog customDialog = new CustomDialog(this);
                customDialog.show();
                break;
            case R.id.fragmentDialogButton:
                CustomFragmentDialog customFragmentDialog = new CustomFragmentDialog();
                customFragmentDialog.show(getFragmentManager());
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("devcpp","MainActivity -> onSaveInstanceState");
        outState.putInt(KEY_SAVE_COUNT, nCount);
        getFragmentManager().findFragmentByTag(CustomFragmentDialog.getFragmentTag()).onSaveInstanceState(outState);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("devcpp","MainActivity -> onConfigurationChanged");

    }
}
