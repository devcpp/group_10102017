package com.kkrasylnykov.l10_dialogsexamples.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;

import com.kkrasylnykov.l10_dialogsexamples.R;

public class CustomDialog extends Dialog implements View.OnClickListener {
    public CustomDialog(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.setCancelable(false);
        findViewById(R.id.textViewDialog).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
