package com.kkrasylnykov.l09_customviewsexample.customviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l09_customviewsexample.R;

public class ImageWithText extends LinearLayout {
    private ImageView imageView;
    private TextView textView;

    public ImageWithText(Context context) {
        super(context);
        init(context, null);
    }

    public ImageWithText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ImageWithText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.view_image_with_text, this, true);
        imageView = findViewById(R.id.imageViewImageWhithText);
        textView = findViewById(R.id.textViewImageWhithText);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int nImageHeight = imageView.getMeasuredHeight();

        Log.d("devcpp", "nImageHeight -> " + nImageHeight);
        while (textView.getMeasuredHeight() > nImageHeight){
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (textView.getTextSize() - 10));
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            Log.d("devcpp", "textView.getMeasuredHeight() -> " + textView.getMeasuredHeight());
        }
    }
}
