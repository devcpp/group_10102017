package com.kkrasylnykov.l09_customviewsexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.kkrasylnykov.l09_customviewsexample.R;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        textView = findViewById(R.id.textViewMainActivity);
//        Log.d("devcpp","textView.getWidth -> " + textView.getWidth());
//        Log.d("devcpp","textView.getHeight -> " + textView.getHeight());
    }

    @Override
    protected void onStart() {
        super.onStart();

//        Log.d("devcpp","onStart -> textView.getWidth -> " + textView.getWidth());
//        Log.d("devcpp","onStart -> textView.getHeight -> " + textView.getHeight());
    }

    @Override
    protected void onResume() {
        super.onResume();

//        Log.d("devcpp","onResume -> textView.getWidth -> " + textView.getWidth());
//        Log.d("devcpp","onResume -> textView.getHeight -> " + textView.getHeight());
    }
}
