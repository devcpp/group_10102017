package com.kkrasylnykov.l09_customviewsexample.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

import com.kkrasylnykov.l09_customviewsexample.R;

public class ResizebleTextView extends TextView {
    private float mMinTextSize = -1;

    public ResizebleTextView(Context context) {
        super(context);
    }

    public ResizebleTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ResizebleTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs){
        if (attrs != null) {
            TypedArray styleAttributes = context.obtainStyledAttributes(attrs, R.styleable.rsizetw);
            if (styleAttributes!=null) {
                for (int i = 0; i < styleAttributes.getIndexCount(); i++){
                    int attr = styleAttributes.getIndex(i);
                    switch (attr){
                        case R.styleable.rsizetw_minTextSize:
                            mMinTextSize = styleAttributes.getDimension(attr, -1);
                            break;
                    }
                }
            }
        }
    }

    private static final int STEP = 5;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        Log.e("devcpp", "onMeasure -> widthMeasureSpec -> " + widthMeasureSpec);
        Log.e("devcpp", "onMeasure -> heightMeasureSpec -> " + heightMeasureSpec);

        int realWidth = MeasureSpec.getSize(widthMeasureSpec);
        Log.e("devcpp", "onMeasure -> getMode -> " + MeasureSpec.getMode(widthMeasureSpec));
        Log.e("devcpp", "onMeasure -> getSize -> " + realWidth);

        Log.e("devcpp", "onMeasure -> mMinTextSize -> " + mMinTextSize);

        int unspecifiedWidth = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        super.onMeasure(unspecifiedWidth, heightMeasureSpec);

        int measureWidth = this.getMeasuredWidth();

        if (measureWidth <= realWidth) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        float delta = ((float)realWidth) / measureWidth;

        float fontSize = this.getTextSize() * delta;

        if (fontSize < mMinTextSize && mMinTextSize != -1) {
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, mMinTextSize);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            this.setEllipsize(TextUtils.TruncateAt.END);
            this.setLines(1);
            return;
        }

        this.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        super.onMeasure(unspecifiedWidth, heightMeasureSpec);

        while (this.getMeasuredWidth() > realWidth) {
            fontSize -= STEP;
            if (fontSize < mMinTextSize && mMinTextSize != -1) {
                this.setTextSize(TypedValue.COMPLEX_UNIT_PX, mMinTextSize);
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                this.setEllipsize(TextUtils.TruncateAt.END);
                this.setLines(1);
                return;
            }
            this.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
            super.onMeasure(unspecifiedWidth, heightMeasureSpec);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
