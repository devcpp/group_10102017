package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout conteiner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        AppDBHelper dbHelper = new AppDBHelper(this);
        conteiner = findViewById(R.id.conteinerLLayoutMainActivity);

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllButtonMainActivity).setOnClickListener(this);

        if (appSettings.isFistStart()) {
            appSettings.setIsFistStart(false);
            //TODO NEED ADD CODE PROCESSING FIRST START
            Toast.makeText(this, "FIRST START", Toast.LENGTH_LONG).show();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if (db != null) {
                for (int i=0; i<100; i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBConstants.DB_FIELD_NAME, "Name_" + i);
                    contentValues.put(DBConstants.DB_FIELD_SNAME, "SName_" + i);
                    contentValues.put(DBConstants.DB_FIELD_PHONE, "095234234" + i);
                    contentValues.put(DBConstants.DB_FIELD_ADDRES, "Devichia str, " + i);
                    contentValues.put(DBConstants.DB_FIELD_BDAY, (long)(i*100 + i*10 + i));

                    db.insert(DBConstants.DB_TABLE_NAME, null, contentValues);
                }

                db.close();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateData();
    }

    private void updateData(){
        conteiner.removeAllViews();
        AppDBHelper dbHelper = new AppDBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (db != null) {
            Log.d("devcpp", "else -> db != null");
            Cursor cursor = db.query(DBConstants.DB_TABLE_NAME, null, null,
                    null, null, null, null);
            if (cursor!=null){
                Log.d("devcpp", "else -> db != null -> cursor!=null");
                if (cursor.moveToFirst()){
                    Log.d("devcpp", "else -> db != null -> cursor!=null -> cursor.moveToFirst()");
                    int nPositionId = cursor.getColumnIndex(DBConstants.DB_FIELD_ID);
                    int nPositionName = cursor.getColumnIndex(DBConstants.DB_FIELD_NAME);
                    int nPositionSName = cursor.getColumnIndex(DBConstants.DB_FIELD_SNAME);
                    int nPositionPhone = cursor.getColumnIndex(DBConstants.DB_FIELD_PHONE);
                    int nPositionAddres = cursor.getColumnIndex(DBConstants.DB_FIELD_ADDRES);
                    int nPositionBDay = cursor.getColumnIndex(DBConstants.DB_FIELD_BDAY);

                    LinearLayout.LayoutParams layoutParams =
                            new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);
                    do{
                        Log.d("devcpp", "do...");

                        long nId = cursor.getLong(nPositionId);
                        String strName = cursor.getString(nPositionName);
                        String strSName = cursor.getString(nPositionSName);
                        String strPhone = cursor.getString(nPositionPhone);
                        String strAddres = cursor.getString(nPositionAddres);
                        long nBDay = cursor.getLong(nPositionBDay);

                        TextView dataTextView = new TextView(this);
                        dataTextView.setPadding(10, 20, 10, 20);
                        dataTextView.setLayoutParams(layoutParams);
                        dataTextView.setText(nId + " " + strName + " " + strSName + "\ntel: " +
                                strPhone + "\naddres: " + strAddres + "\nBDAY: " + nBDay);
                        conteiner.addView(dataTextView, 0);

                        dataTextView.setTag(nId);
                        dataTextView.setOnClickListener(this);
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            db.close();
        }
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag instanceof Long){
            long id = (long) tag;

            Intent editActivityIntent = new Intent(this, EditActivity.class);
            editActivityIntent.putExtra(EditActivity.KEY_USER_ID, id);
            startActivity(editActivityIntent);
        } else {
            switch (v.getId()){
                case R.id.addButtonMainActivity:
                    startActivity(new Intent(this, EditActivity.class));
                    break;
                case R.id.removeAllButtonMainActivity:
                    AppDBHelper dbHelper = new AppDBHelper(this);
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    db.delete(DBConstants.DB_TABLE_NAME, null, null);
                    db.close();

                    updateData();
                    break;
            }
        }


    }
}
