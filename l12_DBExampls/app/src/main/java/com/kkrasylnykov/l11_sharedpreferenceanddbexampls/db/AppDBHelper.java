package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class AppDBHelper extends SQLiteOpenHelper {

    public AppDBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBConstants.DB_TABLE_NAME +
                " ( " + DBConstants.DB_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBConstants.DB_FIELD_NAME + " TEXT NOT NULL, " +
                DBConstants.DB_FIELD_SNAME + " TEXT, " +
                DBConstants.DB_FIELD_PHONE + " TEXT, " +
                DBConstants.DB_FIELD_ADDRES + " TEXT, " +
                DBConstants.DB_FIELD_BDAY + " INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
