package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY_USER_ID = "KEY_USER_ID";

    private EditText nameEditText;
    private EditText snameEditText;
    private EditText phoneEditText;
    private EditText addresEditText;

    private long mUserId = -1;
    private long mBDay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        nameEditText = findViewById(R.id.nameEditTextEditActivity);
        snameEditText = findViewById(R.id.snameEditTextEditActivity);
        phoneEditText = findViewById(R.id.phoneEditTextEditActivity);
        addresEditText = findViewById(R.id.addresEditTextEditActivity);
        //findViewById(R.id.addButtonEditActivity).setOnClickListener(this)

        Button addButton = findViewById(R.id.addButtonEditActivity);
        addButton.setOnClickListener(this);

        Button removeButton = findViewById(R.id.removeButtonEditActivity);
        removeButton.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            mUserId = bundle.getLong(KEY_USER_ID, -1);
        }

        if (mUserId != -1) {
            AppDBHelper dbHelper = new AppDBHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            String strSelection = DBConstants.DB_FIELD_ID + "=?";
            String[] argsSelection = new String[]{Long.toString(mUserId)};
            Cursor cursor = db.query(DBConstants.DB_TABLE_NAME, null, strSelection,
                    argsSelection, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()){
                    int nPositionName = cursor.getColumnIndex(DBConstants.DB_FIELD_NAME);
                    int nPositionSName = cursor.getColumnIndex(DBConstants.DB_FIELD_SNAME);
                    int nPositionPhone = cursor.getColumnIndex(DBConstants.DB_FIELD_PHONE);
                    int nPositionAddres = cursor.getColumnIndex(DBConstants.DB_FIELD_ADDRES);
                    int nPositionBDay = cursor.getColumnIndex(DBConstants.DB_FIELD_BDAY);

                    String strName = cursor.getString(nPositionName);
                    String strSName = cursor.getString(nPositionSName);
                    String strPhone = cursor.getString(nPositionPhone);
                    String strAddres = cursor.getString(nPositionAddres);
                    mBDay = cursor.getLong(nPositionBDay);

                    nameEditText.setText(strName);
                    snameEditText.setText(strSName);
                    phoneEditText.setText(strPhone);
                    addresEditText.setText(strAddres);

                    addButton.setText("Update");
                    removeButton.setVisibility(View.VISIBLE);
                }
                cursor.close();
            }
            db.close();
        }
    }

    @Override
    public void onClick(View v) {
        String strSelection = DBConstants.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(mUserId)};
        switch (v.getId()) {
            case R.id.removeButtonEditActivity:
                SQLiteDatabase db_ = new AppDBHelper(this).getWritableDatabase();
                db_.delete(DBConstants.DB_TABLE_NAME, strSelection, argsSelection);
                finish();
                break;
            case R.id.addButtonEditActivity:
                String strName = nameEditText.getText().toString();
                String strSName = snameEditText.getText().toString();
                String strPhone = phoneEditText.getText().toString();
                String strAddres = addresEditText.getText().toString();

                if (strName.isEmpty()) {
                    Toast.makeText(this, "Name is emplty!!!!", Toast.LENGTH_LONG).show();
                    return;
                }

                ContentValues contentValues = new ContentValues();
                contentValues.put(DBConstants.DB_FIELD_NAME, strName);
                contentValues.put(DBConstants.DB_FIELD_SNAME, strSName);
                contentValues.put(DBConstants.DB_FIELD_PHONE, strPhone);
                contentValues.put(DBConstants.DB_FIELD_ADDRES, strAddres);
                contentValues.put(DBConstants.DB_FIELD_BDAY, mUserId != -1 ? mBDay : (System.currentTimeMillis() / 1000l));

                AppDBHelper dbHelper = new AppDBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                if (mUserId != -1) {
                    db.update(DBConstants.DB_TABLE_NAME, contentValues, strSelection, argsSelection);
                    finish();
                    return;
                } else {
                    db.insert(DBConstants.DB_TABLE_NAME, null, contentValues);
                    nameEditText.setText("");
                    snameEditText.setText("");
                    phoneEditText.setText("");
                    addresEditText.setText("");
                    nameEditText.requestFocus();
                }
                break;
        }
    }
}
