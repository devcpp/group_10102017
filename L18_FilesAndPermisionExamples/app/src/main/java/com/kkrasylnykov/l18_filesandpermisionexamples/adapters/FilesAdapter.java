package com.kkrasylnykov.l18_filesandpermisionexamples.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l18_filesandpermisionexamples.R;

import java.util.ArrayList;

public class FilesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<String> data;
    private OnFileItemClickListener listener;

    public FilesAdapter(ArrayList<String> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file,
                parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemVH itemVH = (ItemVH) holder;
        final String itemData = data.get(position);
        itemVH.textView.setText(itemData);
        itemVH.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    listener.onFileItemClick(itemData);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ItemVH extends RecyclerView.ViewHolder{
        TextView textView;
        public ItemVH(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.infoOfFileTextView);
        }
    }

    public void setOnItemClickListener(OnFileItemClickListener listener){
        this.listener = listener;
    }

    public interface OnFileItemClickListener{
        void onFileItemClick(String path);
    }
}
