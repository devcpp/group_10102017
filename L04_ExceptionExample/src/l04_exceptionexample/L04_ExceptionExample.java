
package l04_exceptionexample;

import java.util.InputMismatchException;
import java.util.Scanner;


public class L04_ExceptionExample {

    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            int[] nMas = new int[]{1, 0, 5, 4, 8, 6};

            int nPos = scanner.nextInt();

            int fRes = 2/nMas[nPos];

            System.out.println(fRes);
            System.out.println(nMas[nPos]);

            float fData = scanner.nextFloat(); 
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Index id BIG!!!!!");
        } catch (ArithmeticException e){
            System.out.println("ArithmeticException!!!!!");
        } catch (InputMismatchException e){
            System.out.println("InputMismatchException!!!!!");
        } catch (Exception e) {
            System.out.println("Exception -> " + e.toString());
            System.out.println("Exception -> " + e.getMessage());
        }
    }
    
}
