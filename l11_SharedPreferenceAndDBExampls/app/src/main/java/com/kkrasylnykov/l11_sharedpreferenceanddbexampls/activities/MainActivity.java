package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        AppDBHelper dbHelper = new AppDBHelper(this);
        if (appSettings.isFistStart()) {
            appSettings.setIsFistStart(false);
            //TODO NEED ADD CODE PROCESSING FIRST START
            Toast.makeText(this, "FIRST START", Toast.LENGTH_LONG).show();

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if (db != null) {
                for (int i=0; i<100; i++){
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DBConstants.DB_FIELD_NAME, "Name_" + i);
                    contentValues.put(DBConstants.DB_FIELD_SNAME, "SName_" + i);
                    contentValues.put(DBConstants.DB_FIELD_PHONE, "095234234" + i);
                    contentValues.put(DBConstants.DB_FIELD_ADDRES, "Devichia str, " + i);
                    contentValues.put(DBConstants.DB_FIELD_BDAY, (long)(i*100 + i*10 + i));

                    db.insert(DBConstants.DB_TABLE_NAME, null, contentValues);
                }

                db.close();
            }
        } else {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            if (db != null) {
                Cursor cursor = db.query(DBConstants.DB_TABLE_NAME, null, null,
                        null, null, null, null);
                if (cursor!=null){
                    if (cursor.moveToFirst()){
                        int nPositionId = cursor.getColumnIndex(DBConstants.DB_FIELD_ID);
                        int nPositionName = cursor.getColumnIndex(DBConstants.DB_FIELD_NAME);
                        int nPositionSName = cursor.getColumnIndex(DBConstants.DB_FIELD_SNAME);
                        int nPositionPhone = cursor.getColumnIndex(DBConstants.DB_FIELD_PHONE);
                        int nPositionAddres = cursor.getColumnIndex(DBConstants.DB_FIELD_ADDRES);
                        int nPositionBDay = cursor.getColumnIndex(DBConstants.DB_FIELD_BDAY);

                        do{
                            long nId = cursor.getLong(nPositionId);
                            String strName = cursor.getString(nPositionName);
                            String strSName = cursor.getString(nPositionSName);
                            String strPhone = cursor.getString(nPositionPhone);
                            String strAddres = cursor.getString(nPositionAddres);
                            long nBDay = cursor.getLong(nPositionBDay);

                            Log.d("devcpp", nId + " -> " + strName + " -> " + strPhone);
                        } while (cursor.moveToNext());


                    }
                    cursor.close();
                }
                db.close();
            }
        }
    }
}
