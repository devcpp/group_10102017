package com.kkrasylnykov.l25_servicesandbroadcastexamples.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.kkrasylnykov.l25_servicesandbroadcastexamples.R;
import com.kkrasylnykov.l25_servicesandbroadcastexamples.services.LoadService;

import java.io.File;

public class LoadActivity extends AppCompatActivity {

    private static final String URL_FILE = "http://50.7.127.154/s/7c926e0e55002670ac05b25255715503/teorijabolshogovzriva/s01e01_480.mp4";
    private static final String LOCAL_FILE_NAME = "s01e01_480.mp4";

    private TextView infoTextView;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (infoTextView != null && intent != null && intent.getExtras() != null) {
                infoTextView.setText(intent.getStringExtra(LoadService.DATA));
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(LoadService.ACTION + LOCAL_FILE_NAME));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        infoTextView = findViewById(R.id.infoOfFileTextViewLoadActivity);

        File file = new File(getFilesDir(), LOCAL_FILE_NAME);
        if (file.exists()) {
            infoTextView.setText("File is load -> " + file.length());
            file.delete();
        } else {
            startService(LoadService.getInstance(this, URL_FILE, LOCAL_FILE_NAME));
        }
    }
}
