package com.kkrasylnykov.l25_servicesandbroadcastexamples.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l25_servicesandbroadcastexamples.activities.MainActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class LoadService extends Service {
    public static final String ACTION = "com.kkrasylnykov.l25_servicesandbroadcastexamples.services.Service";
    public static final String DATA = "DATA";

    private static final String KEY_FILE_URL = "KEY_FILE_URL";
    private static final String KEY_FILE_NAME = "KEY_FILE_NAME";

    private static ArrayList<Thread> arrThreads;
    private static ArrayList<String> arrNames;

    private int nCount = 0;

    public static Intent getInstance(Context context, String url, String fileName){
        Intent intent = new Intent(context, LoadService.class);
        intent.putExtra(KEY_FILE_URL, url);
        intent.putExtra(KEY_FILE_NAME, fileName);
        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (arrThreads==null) {
            arrThreads = new ArrayList<>();
            arrNames = new ArrayList<>();
        }

        Log.d("devcpp","LoadService -> onCreate");
    }

    private void customStopSelf(){
        for (Thread item : arrThreads) {
            if (item.isAlive()) {
                item.interrupt();
            }
        }
        stopSelf();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcpp","LoadService -> onStartCommand -> " + intent);
        Log.d("devcpp","LoadService -> onStartCommand -> " + flags);
        Log.d("devcpp","LoadService -> onStartCommand -> " + startId);
        if (intent == null){
            Toast.makeText(this, "LoadService -> onStartCommand -> stopSelf", Toast.LENGTH_LONG).show();
            customStopSelf();
        } else {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String url = bundle.getString(KEY_FILE_URL, "");
                String fileName = bundle.getString(KEY_FILE_NAME, "");
                if (!arrNames.contains(fileName)) {
                    arrNames.add(fileName);
                    Thread thread = new Thread(new LoadRunnable(url, fileName, new LoadFinishInterface(){
                        @Override
                        public void onLoadFinish() {
                            if (--nCount == 0) {
                                customStopSelf();
                            }
                        }
                    }));
                    arrThreads.add(thread);
                    thread.start();
                }

                nCount++;
            } else {
                customStopSelf();
            }

        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("devcpp","LoadService -> onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class LoadRunnable implements Runnable {

        private String url;
        private String fileName;
        private LoadFinishInterface loadFinish;

        public LoadRunnable(String url, String fileName, LoadFinishInterface loadFinish) {
            this.url = url;
            this.fileName = fileName;
            this.loadFinish = loadFinish;
        }

        @Override
        public void run() {
            try {
                URL url = new URL(this.url);
                URLConnection conection = url.openConnection();
                conection.connect();

                int lenghtOfFile = conection.getContentLength();

                InputStream input = new BufferedInputStream(url.openStream(),
                        4096);
                File file = new File(LoadService.this.getFilesDir(), this.fileName + "_tmp");
                OutputStream output =
                        new FileOutputStream(file);

                byte data[] = new byte[2048];
                float total = 0;
                int count;
                int nOldProgress = 0;
                while ((count = input.read(data)) != -1) {
                    if (Thread.currentThread().isInterrupted()){
                        Log.e("devcpp","isInterrupted!!!!");
                        output.flush();

                        output.close();
                        input.close();

                        if (file.exists()) {
                            file.delete();
                        }
                        return;
                    }
                    total += count;
                    int nDelta = (int) ((total/lenghtOfFile) * 100);
                    if (nOldProgress != nDelta) {
                        nOldProgress = nDelta;
                        Log.d("devcpp","progress -> " + nDelta);
                        sendData("progress -> " + nDelta);
                    }

                    output.write(data, 0, count);
                }

                output.flush();

                output.close();
                input.close();

                file.renameTo(new File(LoadService.this.getFilesDir(), this.fileName));

                sendData("finish load " + this.fileName);

                this.loadFinish.onLoadFinish();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        private void sendData(String data){
            Intent intent = new Intent();
            intent.setAction(ACTION + this.fileName);
            intent.putExtra(DATA, data);
            sendBroadcast(intent);
        }
    }

    public interface LoadFinishInterface {
        void onLoadFinish();
    }
}
