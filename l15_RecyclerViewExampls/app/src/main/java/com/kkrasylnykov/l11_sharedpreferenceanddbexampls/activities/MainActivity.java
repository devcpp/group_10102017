package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.adapters.UserInfoAdapter;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.adapters.UserInfoRecyclerAdapter;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines.UserEngine;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        UserInfoRecyclerAdapter.OnClickOnListListener {

//    private LinearLayout conteiner;
//    private ListView listView;
//    private UserInfoAdapter adapter;

    private RecyclerView recyclerView;
    private UserInfoRecyclerAdapter adapter;
//    private UserInfoRecyclerAdapter.OnClickOnListListener listener = new UserInfoRecyclerAdapter.OnClickOnListListener() {
//        @Override
//        public void onClickItem(long id) {
//
//        }
//
//        @Override
//        public void onClickButton(String name) {
//
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        recyclerView = findViewById(R.id.conteinerRecyclerViewMainActivity);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UserInfoRecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setOnClickOnListListener(this);
//        adapter.setOnClickOnListListener(new UserInfoRecyclerAdapter.OnClickOnListListener() {
//            @Override
//            public void onClickItem(long id) {
//
//            }
//
//            @Override
//            public void onClickButton(String name) {
//
//            }
//        });

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.addOneButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeOneButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.changeOneButtonMainActivity).setOnClickListener(this);

        if (appSettings.isFistStart()) {
            Log.d("devcpp","isFistStart -> start");
            appSettings.setIsFistStart(false);
            //TODO NEED ADD CODE PROCESSING FIRST START
            Toast.makeText(this, "FIRST START", Toast.LENGTH_LONG).show();

            UserEngine userEngine = new UserEngine(this);
            for (int i=0; i<10000; i++){
                UserEntity userEntity = new UserEntity("Name_", "SName_",
                        "095234234", "Devichia str, ",
                        (long)(i));
                userEngine.insertUser(userEntity);
            }
            Log.d("devcpp","isFistStart -> end");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateData();
    }

    private void updateData(){
        UserEngine userEngine = new UserEngine(this);
        users = userEngine.getAllUsers();
        adapter.updateData(users);
    }
    ArrayList<UserEntity> users;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addOneButtonMainActivity:
                users.add(2, new UserEntity("Kos", "Test",
                        "112-05-06-07", "Address", 0));
                adapter.updateDataWithoutNotification(users);
                adapter.notifyItemInserted(3);
                break;
            case R.id.removeOneButtonMainActivity:
                users.remove(2);
                adapter.updateDataWithoutNotification(users);
                adapter.notifyItemRemoved(3);
                break;
            case R.id.changeOneButtonMainActivity:
                users.get(2).setPhone("911-02-03-01");
                adapter.updateDataWithoutNotification(users);
                adapter.notifyItemChanged(3);
                break;
            case R.id.addButtonMainActivity:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAllButtonMainActivity:
                AppDBHelper dbHelper = new AppDBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(DBConstants.DB_TABLE_NAME, null, null);
                db.close();

                updateData();
                break;
        }
    }

    @Override
    public void onClickItem(UserEntity item) {
        Intent editActivityIntent = new Intent(this, EditActivity.class);
        editActivityIntent.putExtra(EditActivity.KEY_USER_ID, item.getUserId());
        startActivity(editActivityIntent);
    }

    @Override
    public void onClickButton(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }
}
