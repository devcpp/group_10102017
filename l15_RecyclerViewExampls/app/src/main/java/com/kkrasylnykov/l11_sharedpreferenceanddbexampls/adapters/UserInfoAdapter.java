package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;

import java.util.ArrayList;

public class UserInfoAdapter extends BaseAdapter {

    private ArrayList<UserEntity> data;
    private OnClickOnListListener listener;

    public UserInfoAdapter(){
        data = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((UserEntity)getItem(position)).getUserId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_user_info, parent, false);
            viewHolder = new UserViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (UserViewHolder) convertView.getTag();
        }
        final UserEntity item = (UserEntity) getItem(position);

        viewHolder.nameTextView.setText(item.getUserId() + " -> " + item.getName() + " " + item.getSName());
        viewHolder.phoneTextView.setText("tel: " + item.getPhone());
        viewHolder.addresTextView.setText("adr: " + item.getAddres());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null){
                    listener.onClickItem(item.getUserId());
                }
            }
        });
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null){
                    listener.onClickButton(item.getPhone());
                }
            }
        });
        return convertView;
    }

    public void updateData(ArrayList<UserEntity> data){
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void setOnClickListListener(OnClickOnListListener listener){
        this.listener = listener;
    }

    class UserViewHolder{
        TextView nameTextView;
        TextView phoneTextView;
        TextView addresTextView;
        Button button;

        UserViewHolder(View view){
            this.nameTextView = view.findViewById(R.id.nameTextViewUserItem);
            this.phoneTextView = view.findViewById(R.id.phoneTextViewUserItem);
            this.addresTextView = view.findViewById(R.id.adrresTextViewUserItem);
            this.button = view.findViewById(R.id.clickButtonUserItem);
        }
    }

    public interface OnClickOnListListener{
        void onClickItem(long id);
        void onClickButton(String name);
    }
}
