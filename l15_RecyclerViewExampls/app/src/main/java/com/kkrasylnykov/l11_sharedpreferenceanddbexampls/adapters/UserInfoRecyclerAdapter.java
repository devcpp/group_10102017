package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;

import java.util.ArrayList;

public class UserInfoRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 1001;
    private static final int TYPE_HEADER = 1002;
    private static final int TYPE_FOOTER = 1003;

    private ArrayList<UserEntity> data;
    private @Nullable OnClickOnListListener listener;

    public UserInfoRecyclerAdapter(){
        data = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        int result = TYPE_ITEM;
        if (position == 0) {
            result = TYPE_HEADER;
        } else if (position == (getItemCount() - 1)){
            result = TYPE_FOOTER;
        }
        return result;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        switch (viewType){
            case TYPE_ITEM:
                holder = new UserItemViewHolder(LayoutInflater.from(parent.getContext()).
                        inflate(R.layout.item_user_info, parent, false));
                break;
            case TYPE_HEADER:
                holder = new HeaderViewHolder(LayoutInflater.from(parent.getContext()).
                        inflate(R.layout.item_header, parent, false));
                break;
            case TYPE_FOOTER:
                holder = new FooterViewHolder(LayoutInflater.from(parent.getContext()).
                        inflate(R.layout.item_footer, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int nType = getItemViewType(position);
//        holder.setIsRecyclable(false);

        if (nType == TYPE_ITEM){
            UserItemViewHolder userItemViewHolder = (UserItemViewHolder) holder;
            final UserEntity item = data.get(position-1);

            userItemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClickItem(item);
                    }

                }
            });

            userItemViewHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClickButton(item.getName());
                    }

                }
            });

            userItemViewHolder.nameTextView.setText(item.getUserId() + " -> " + item.getName() + " " + item.getSName());
            userItemViewHolder.phoneTextView.setText("tel: " + item.getPhone());
            userItemViewHolder.addresTextView.setText("adr: " + item.getAddres());
        } else if (nType == TYPE_HEADER) {
            ((HeaderViewHolder) holder).countTextView.setText(Integer.toString(data.size()));
        }

//        if (holder instanceof UserItemViewHolder){
//
//        }
    }

    @Override
    public int getItemCount() {
        return data.size() + 2;
    }

    public void updateData(ArrayList<UserEntity> data){
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void updateDataWithoutNotification(ArrayList<UserEntity> data){
        this.data.clear();
        this.data.addAll(data);
    }

    public void setOnClickOnListListener(@Nullable OnClickOnListListener listener){
        this.listener = listener;
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder{

        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView countTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            countTextView = itemView.findViewById(R.id.countTextViewHeaderItem);
        }
    }

    private class UserItemViewHolder extends RecyclerView.ViewHolder{
        TextView nameTextView;
        TextView phoneTextView;
        TextView addresTextView;
        Button button;

        public UserItemViewHolder(View itemView) {
            super(itemView);
            this.nameTextView = itemView.findViewById(R.id.nameTextViewUserItem);
            this.phoneTextView = itemView.findViewById(R.id.phoneTextViewUserItem);
            this.addresTextView = itemView.findViewById(R.id.adrresTextViewUserItem);
            this.button = itemView.findViewById(R.id.clickButtonUserItem);
        }
    }

    public interface OnClickOnListListener{
        void onClickItem(UserEntity item);
        void onClickButton(String name);
    }
}
