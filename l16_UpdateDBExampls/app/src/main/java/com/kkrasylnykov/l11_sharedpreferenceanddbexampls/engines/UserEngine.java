package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines;

import android.content.Context;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers.UserDBWrapper;

import java.util.ArrayList;

public class UserEngine {

    private Context mContext;

    public UserEngine(Context context){
        this.mContext = context;
    }

    public ArrayList<UserEntity> getAllUsers(){
        UserDBWrapper userDBWrapper = new UserDBWrapper(mContext);
        return userDBWrapper.getAllUsers();
    }

    public UserEntity getUserById(long nId){
        UserDBWrapper userDBWrapper = new UserDBWrapper(mContext);
        return userDBWrapper.getUserById(nId);
    }

    public void insertUser(UserEntity user){
        UserDBWrapper userDBWrapper = new UserDBWrapper(mContext);
        userDBWrapper.insertUser(user);
    }

    public void updateUser(UserEntity user) {
        UserDBWrapper userDBWrapper = new UserDBWrapper(mContext);
        userDBWrapper.updateUser(user);
    }

    public void removeUser(UserEntity user) {
        removeUserById(user.getUserId());
    }

    public void removeUserById(long nId) {
        UserDBWrapper userDBWrapper = new UserDBWrapper(mContext);
        userDBWrapper.removeUser(nId);
    }

    public void removeAllUsers(){
        UserDBWrapper userDBWrapper = new UserDBWrapper(mContext);
        userDBWrapper.removeAllUsers();
    }
}
