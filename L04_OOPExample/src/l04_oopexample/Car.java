package l04_oopexample;

import java.util.Scanner;

public class Car extends BaseTransport{
    
    private String mTypeBody;

    @Override
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Manufacture Name: ");
        String strManufactureName = scanner.nextLine();
        
        System.out.print("Enter Model Name: ");
        String strModelName = scanner.nextLine();
        
        System.out.print("Enter Power: ");
        float fPower = scanner.nextFloat();
        
        scanner.nextLine();
        System.out.print("Enter Body Type: ");
        String strBodyType = scanner.nextLine();
        
        System.out.print("Enter Wheel Count: ");
        int nWhellCount = scanner.nextInt();
        
        setManufactureName(strManufactureName);
        setModelName(strModelName);
        setPower(fPower);
        setTypeBody(strBodyType);
        setCountWheel(nWhellCount);
    }

    @Override
    public void outputData() {
        System.out.println("\t\t--CAR INFO--");
        System.out.println("Manufacture Name:\t" + getManufactureName());
        System.out.println("Model Name:\t\t" + getModelName());
        System.out.println("Power:\t\t\t" + getPower());
        System.out.println("Body Type:\t\t" + getTypeBody());
        System.out.println("Wheel Count:\t\t" + getCountWheel());
    }

    @Override
    public boolean search(String strSearch) {
        return getManufactureName().toLowerCase().contains(strSearch) ||
                getModelName().toLowerCase().contains(strSearch) ||
                getTypeBody().toLowerCase().contains(strSearch);
    }

    public String getTypeBody() {
        return mTypeBody;
    }

    public void setTypeBody(String mTypeBody) {
        this.mTypeBody = mTypeBody;
    }    
}
