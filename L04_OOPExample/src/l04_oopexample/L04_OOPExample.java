package l04_oopexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L04_OOPExample {
    
    private static ArrayList<BaseTransport> arrData;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        arrData = new ArrayList<BaseTransport>();
        
        do{
            System.out.println("Menu:");
            System.out.println("1. Add item.");
            System.out.println("2. Show all items.");
            System.out.println("3. Search.");
            System.out.println("4. Remove all items.");
            System.out.println("0. Exit.");
            
            int n = scanner.nextInt();
            
            switch (n){
                case 1:
                    addItems();
                    break;
                case 2:
                    showAllItems();
                    break;
                case 3:
                    search();
                    break;
                case 4:
                    arrData.clear();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Invalid!!!!!");
            }
            
        }while(true);
    }
    
    private static void showAllItems(){
        for(BaseTransport item : arrData){
            if (item instanceof Car){
                System.out.println("This is car -> ");
            }
            item.outputData();
        }
    }
    
    private static void search(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter search text: ");
        String strSearch = scanner.nextLine();
        for(BaseTransport item : arrData){
            if (item.search(strSearch)){
                item.outputData();
            }            
        }
    }
    
    private static void addItems(){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println("Menu:");
            System.out.println("1. Car.");
            System.out.println("2. Moto");
            System.out.println("0. Exit.");
            
            int n = scanner.nextInt();
            BaseTransport transport = null;
            switch (n){
                case 1:
                    transport = new Car();
                    break;
                case 2:
                    transport = new Moto();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Invalid!!!!!");
            }
            
            if (transport != null){
                transport.inputData();
                arrData.add(transport);
                return;
            }         
        }while(true);
    }
    
}
