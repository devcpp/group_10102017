package l04_oopexample;

public abstract class BaseTransport {
    
    private String mManufactureName;
    private String mModelName;
    private float mPower;
    private int mCountWheel;

    public String getManufactureName() {
        return mManufactureName;
    }

    public void setManufactureName(String mManufactureName) {
        this.mManufactureName = mManufactureName;
    }

    public String getModelName() {
        return mModelName;
    }

    public void setModelName(String mModelName) {
        this.mModelName = mModelName;
    }

    public float getPower() {
        return mPower;
    }

    public void setPower(float mPower) {
        this.mPower = mPower;
    }

    public int getCountWheel() {
        return mCountWheel;
    }

    public void setCountWheel(int mCountWheel) {
        this.mCountWheel = mCountWheel;
    }
    
    public abstract void inputData();
    
    public abstract void outputData();
    
    public abstract boolean search(String strSearch);
    
}
