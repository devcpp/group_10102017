package l04_oopexample;

import java.util.Scanner;

public class Moto extends BaseTransport{
    
    private float mWidth;

    @Override
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Moto Manufacture Name: ");
        String strManufactureName = scanner.nextLine();
        
        System.out.print("Enter Moto Model Name: ");
        String strModelName = scanner.nextLine();
        
        System.out.print("Enter Moto Power: ");
        float fPower = scanner.nextFloat();
        
        System.out.print("Enter Moto Width: ");
        float fWidth = scanner.nextFloat();
        
        System.out.print("Enter Moto Wheel Count: ");
        int nWhellCount = scanner.nextInt();
        
        setManufactureName(strManufactureName);
        setModelName(strModelName);
        setPower(fPower);
        setWidth(fWidth);
        setCountWheel(nWhellCount);
    }

    @Override
    public void outputData() {
        System.out.println("\t\t--MOTO INFO--");
        System.out.println("Manufacture Name:\t" + getManufactureName());
        System.out.println("Model Name:\t\t" + getModelName());
        System.out.println("Power:\t\t\t" + getPower());
        System.out.println("Width:\t\t\t" + getWidth());
        System.out.println("Wheel Count:\t\t" + getCountWheel());
    }

    @Override
    public boolean search(String strSearch) {
        return getManufactureName().toLowerCase().contains(strSearch) ||
                getModelName().toLowerCase().contains(strSearch);
    }

    public float getWidth() {
        return mWidth;
    }

    public void setWidth(float mWidth) {
        this.mWidth = mWidth;
    }
}
