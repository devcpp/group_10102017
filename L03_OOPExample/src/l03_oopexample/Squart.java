package l03_oopexample;

public class Squart {
    private float mSide;
    private boolean mIsValid;
    private static int count = 0;
    
    static {
        count = 100;
    }
    
    public Squart(){
        this(0f);
        System.out.println("Squart -> Squart()");
    }
    
    public Squart(float side){
        System.out.println("Squart -> Squart(float side) -> " + side);
        mIsValid = false;
        setSide(side);
        System.out.println("Squart.count -> " + ++count);
    }
    
    public static int getCount(){
        return count;
    }

    @Override
    public String toString() {
        String strResult = "side = " + getSide() + 
                "\nP = " + getP() + "\nS=" + getS();
        return strResult;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj instanceof Squart){
            Squart objSquart = (Squart) obj;
            bResult = objSquart.getSide() == this.getSide();
        } else if (obj instanceof Float){
            Float objFloat = (Float) obj;
            bResult = objFloat == this.getSide();
        }
        return bResult;
    }
    
    public void setDiahonal(float diahonal){
        float side = (float) (diahonal/Math.pow(2, 0.5));
        setSide(side);
    }
    
    public float getP(){
        return getSide() * 4;
    }
    
    public float getS(){
        return getSide() * getSide();
    }
    
    public void setSide(float side){
        if (side > 0){
            mSide = side;
        } else {
           side = 0; 
        }
    }
    
    public float getSide(){
        return mSide;
    }
}
