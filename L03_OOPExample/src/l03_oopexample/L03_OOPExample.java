package l03_oopexample;

import java.util.Scanner;

public class L03_OOPExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Squart.getCount() -> " + Squart.getCount());
        
        Squart sq1 = new Squart();
        Squart sq2 = new Squart();
        Squart sq3 = null;
        float side = scanner.nextFloat();
        sq1.setSide(side);
        sq2.setSide(side);
        
        System.out.println("sq1 -> getSide -> " + sq1.getSide());
        System.out.println("sq1 -> getP -> " + sq1.getP());
        System.out.println("sq1 -> getS -> " + sq1.getS());
        
        System.out.println("sq1 -> getS -> " + sq1);
        
        double d = Math.PI + 2;
        
        
        System.out.println("sq1.equals(sq2) -> " + sq1.equals(sq2));
        System.out.println("(sq1 == sq2) -> " + (sq1 == sq2));
        System.out.println("sq1.equals(2) -> " + sq1.equals(2));
        System.out.println("sq1.equals(10f) -> " + sq1.equals(10f));
        System.out.println("sq1.equals(null) -> " + sq1.equals(null));
        
        System.out.println("Squart.getCount() -> " + Squart.getCount());
        //System.out.println("sq3.equals(10f) -> " + sq3.equals(10f));
        
        /*float s = 5000;
        float side2 = (float) Math.pow(s, 0.5);*/
        
    }
    
    /*private class SubClassExample {
        
    }*/
    
}
