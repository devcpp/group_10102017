package com.kkrasylnykov.l18_filesandpermisionexamples.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.kkrasylnykov.l18_filesandpermisionexamples.R;

public class ImageViewFragment extends Fragment {

    public static final String KEY_PATH = "KEY_PATHS";

    private ImageView imageView;
    private ProgressBar progressBar;
    private String path = null;
    private Thread thread;
    private boolean isStoped = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_view, container, false);
        Bundle bundle = getArguments();
        if (bundle != null){
            path = bundle.getString(KEY_PATH, null);
        }
        imageView = view.findViewById(R.id.imageViewFragment);
        progressBar = view.findViewById(R.id.progressBarViewFragment);

        progressBar.setVisibility(View.VISIBLE);

        Log.d("devcpp"," -> "
                + Thread.currentThread().getName());

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (Thread.currentThread().isInterrupted()){
                    return;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, options);

                if (Thread.currentThread().isInterrupted()){
                    return;
                }

                Display display = ((AppCompatActivity)getContext()).getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;

                if (Thread.currentThread().isInterrupted()){
                    return;
                }

                int nDelta = (options.outWidth / width) + 1;
                options = new BitmapFactory.Options();
                options.inSampleSize = nDelta;

                if (Thread.currentThread().isInterrupted()){
                    return;
                }

                final Bitmap bitmap = BitmapFactory.decodeFile(path, options);

                isStoped = true;
                Log.e("devcpp","Thread ---- isStoped!!!!!!!");
                Log.d("devcpp","bitmap -> " + bitmap.getWidth() + " -> "
                        + Thread.currentThread().getName());

                if (Thread.currentThread().isInterrupted() || getActivity() == null || !isAdded()){
                    return;
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
//        thread.setDaemon(true);
        thread.start();

        int i=0;
        while (!isStoped){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i("devcpp"," -> " + isStoped);
            //
        }

        Log.d("devcpp","isStoped!!!!!!!");

//        AsyncTask<String, Integer, Bitmap> loadBitmap = new AsyncTask<String, Integer, Bitmap>() {
//            @Override
//            protected void onPreExecute() {
//                super.onPreExecute();
//
//                progressBar.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            protected Bitmap doInBackground(String... strings) {
//                  BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(path, options);
//
//        Display display = ((AppCompatActivity)getContext()).getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//
//        int nDelta = (options.outWidth / width) + 1;
//        options = new BitmapFactory.Options();
//        options.inSampleSize = nDelta;
//
//        final Bitmap bitmap = BitmapFactory.decodeFile(path, options);
//        Log.d("devcpp","bitmap -> " + bitmap.getWidth());
//
//                return bitmap;
//            }
//
//            @Override
//            protected void onPostExecute(Bitmap bitmap) {
//                super.onPostExecute(bitmap);
//                  imageView.setImageBitmap(bitmap);
//                  progressBar.setVisibility(View.GONE);
//            }
//        };

//        loadBitmap.execute(path);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        thread.interrupt();
    }
}
