package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class BaseEntity {

    public abstract long getId();

    public abstract ContentValues getContentValues();
}
