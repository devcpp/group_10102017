package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserEntity extends BaseEntity {

    private long mUserId;
    private String mName;
    private String mSName;
    private ArrayList<PhoneEntity> mPhones;
    private String mAddres;
    private long mBDay;
    private boolean isCollapsed = true;

    public UserEntity(Cursor cursor){
        int nPositionId = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_ID);
        int nPositionName = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_NAME);
        int nPositionSName = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_SNAME);
        int nPositionAddres = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_ADDRES);
        int nPositionBDay = cursor.getColumnIndex(DBConstants.DbV2.TableUsers.DB_FIELD_BDAY);

        this.mUserId = cursor.getLong(nPositionId);
        this.mName = cursor.getString(nPositionName);
        this.mSName = cursor.getString(nPositionSName);
        this.mAddres = cursor.getString(nPositionAddres);
        this.mBDay = cursor.getLong(nPositionBDay);
    }

    public UserEntity(String mName, String mSName, ArrayList<PhoneEntity> phones,
                      String mAddres, long mBDay) {
        this(-1, mName, mSName, phones, mAddres, mBDay);
    }

    public UserEntity(long mUserId, String mName, String mSName, ArrayList<PhoneEntity> phones,
                      String mAddres, long mBDay) {
        this.mUserId = mUserId;
        this.mName = mName;
        this.mSName = mSName;
        this.mPhones = phones;
        this.mAddres = mAddres;
        this.mBDay = mBDay;
    }

    @Override
    public long getId() {
        return mUserId;
    }

    public void setUserId(long mUserId) {
        this.mUserId = mUserId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getSName() {
        return mSName;
    }

    public void setSName(String mSName) {
        this.mSName = mSName;
    }

    public ArrayList<PhoneEntity> getPhones() {
        return mPhones;
    }

    public void setPhones(ArrayList<PhoneEntity> mPhones) {
        this.mPhones = mPhones;
    }

    public String getAddres() {
        return mAddres;
    }

    public void setAddres(String mAddres) {
        this.mAddres = mAddres;
    }

    public long getBDay() {
        return mBDay;
    }

    public void setBDay(long mBDay) {
        this.mBDay = mBDay;
    }

    public boolean isCollapsed() {
        return isCollapsed;
    }

    public void setCollapsed(boolean collapsed) {
        isCollapsed = collapsed;
    }

    @Override
    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_NAME, getName());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_SNAME, getSName());
//        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_PHONE, getPhone());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_ADDRES, getAddres());
        contentValues.put(DBConstants.DbV2.TableUsers.DB_FIELD_BDAY, getBDay());
        return contentValues;
    }
}
