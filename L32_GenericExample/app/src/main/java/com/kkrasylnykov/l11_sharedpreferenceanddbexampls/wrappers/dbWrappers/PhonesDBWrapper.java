package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.PhoneEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class PhonesDBWrapper extends BaseDBWrapper<PhoneEntity> {

    public PhonesDBWrapper(Context context) {
        super(context, DBConstants.DbV2.TablePhones.DB_TABLE_NAME);
    }

    public ArrayList<PhoneEntity> getPhonesByUserId(long userId){
        ArrayList<PhoneEntity> phoneEntities = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(userId)};// "" + userId
        Cursor cursor = db.query(getTableName(), null, strRequest, arrArgs,
                null, null, null);
        if (cursor != null){
            if (cursor.moveToFirst()) {
                do {
                    phoneEntities.add(new PhoneEntity(cursor));
                } while(cursor.moveToNext());

            }
            cursor.close();
        }
        db.close();
        return phoneEntities;
    }

    public void insert(PhoneEntity... entities) {
        for (PhoneEntity entity : entities) {
            insert(entity);
        }
    }

    public void removePhonesByUserId(long userId){
        if (userId == -1) {
            throw new RuntimeException("User Id = -1! removePhonesByUserId Error!");
        }
        SQLiteDatabase db = getWritableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(userId)};
        db.delete(getTableName(), strRequest, arrArgs);
        db.close();
    }

    @Override
    protected PhoneEntity getNewClass(Cursor cursor) {
        return new PhoneEntity(cursor);
    }
}
