package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.BaseEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public abstract class BaseDBWrapper<T extends BaseEntity> {

    private AppDBHelper mDBHelper;
    private String mTableName;

    public BaseDBWrapper(Context context, String strTableName) {
        mDBHelper = new AppDBHelper(context);
        this.mTableName = strTableName;
    }

    public SQLiteDatabase getWritableDB() {
        return mDBHelper.getWritableDatabase();
    }

    public SQLiteDatabase getReadableDB() {
        return mDBHelper.getReadableDatabase();
    }

    public String getTableName(){
        return mTableName;
    }

    public @NonNull
    ArrayList<T> getAllUsers(){
        ArrayList<T> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        if (db != null) {
            Cursor cursor = db.query(getTableName(), null, null,
                    null, null, null, null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    do{
                        result.add(0, getNewClass(cursor));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            db.close();
        }
        return result;
    }

    public T getItemById(long nId){
        T result = null;
        SQLiteDatabase db = getReadableDB();
        String strSelection = DBConstants.DbV1.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(getTableName(), null, strSelection,
                argsSelection, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()){
                result = getNewClass(cursor);
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public long insert(T user){
        SQLiteDatabase db = getWritableDB();
        long userId = db.insert(getTableName(), null, user.getContentValues());
        db.close();
        return userId;
    }

    public void update(T user) {
        SQLiteDatabase db = getWritableDB();
        String strSelection = DBConstants.DbV1.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(user.getId())};
        db.update(getTableName(), user.getContentValues(), strSelection, argsSelection);
        db.close();
    }

    public void remove(T item) {
        SQLiteDatabase db = getWritableDB();
        String strRequest = DBConstants.DbV2.TablePhones.DB_FIELD_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getId())};
        db.delete(getTableName(), strRequest, arrArgs);
        db.close();
    }

    public void remove(long nId) {
        SQLiteDatabase db = getWritableDB();
        String strSelection = DBConstants.DbV1.DB_FIELD_ID + "=?";
        String[] argsSelection = new String[]{Long.toString(nId)};
        db.delete(getTableName(), strSelection, argsSelection);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();
        db.delete(getTableName(), null, null);
        db.close();
    }

    protected abstract T getNewClass(Cursor cursor);
}
