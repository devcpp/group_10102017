package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.wrappers.dbWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserDBWrapper extends BaseDBWrapper<UserEntity> {

    public UserDBWrapper(Context context) {
        super(context, DBConstants.DbV2.TableUsers.DB_TABLE_NAME);
    }

    @Override
    protected UserEntity getNewClass(Cursor cursor) {
        return new UserEntity(cursor);
    }
}
