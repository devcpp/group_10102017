package com.kkrasylnykov.l05_androidexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mInfoEditText;
    TextView mShowTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInfoEditText = (EditText) findViewById(R.id.infoEditTextMainActivity);
        mShowTextView = (TextView) findViewById(R.id.showTextViewMainActivity);

        Button okButton = (Button) findViewById(R.id.okButtonMainActivity);
        okButton.setOnClickListener(this);

        Button cancelButton = (Button) findViewById(R.id.cancelButtonMainActivity);
        cancelButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.okButtonMainActivity:
                String strText = mInfoEditText.getText().toString().trim();
                if (strText.isEmpty()){
                    mShowTextView.setText("Вы ничего не ввели!!!!! Не надо так((((");
                } else {
                    mShowTextView.setText("Вы ввели: " + strText);
                }
                break;
            case R.id.cancelButtonMainActivity:
                mShowTextView.setText("");
                mInfoEditText.setText("");
                break;
        }
    }
}
