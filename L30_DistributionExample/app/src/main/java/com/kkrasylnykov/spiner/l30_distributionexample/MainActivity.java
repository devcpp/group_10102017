package com.kkrasylnykov.spiner.l30_distributionexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);

        // TODO: Move this to where you establish a user session
        logUser();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                findViewById(R.id.end).getId();
                break;
            case R.id.button2:
                for (int i=0;i<500;i++){
                    Crashlytics.log("R.id.button2 -> int b = 1 / n; -> " + i);
                }

                int n = 1;
                int b = 1 / n;
                break;
            case R.id.button3:
                int arr[] = new int[1];
                arr[2] = 0;
                break;
            case R.id.button4:
                throw new RuntimeException("User click on button4!!!! ><");
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier("12345");
        Crashlytics.setUserEmail("user@fabric.io");
        Crashlytics.setUserName("Test User");
    }

}
