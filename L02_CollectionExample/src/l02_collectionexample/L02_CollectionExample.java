package l02_collectionexample;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;



public class L02_CollectionExample {
    
    public static void inputArray(List<Integer> arrInput){
        for (Integer item : arrInput){
            System.out.print(item + " ");
        }
        System.out.println("");
    }
    
    public static void inputSize(List<Integer> arrInput){
        int n = arrInput.size();
        System.out.println("n = " + n);
    }
    
    public static void inputArrayAndSize(List<Integer> arrInput){
        inputArray(arrInput);
        inputSize(arrInput);
    }

    public static void main(String[] args) {
        //ArrayList<Integer> arrTest = new ArrayList<>(); //new ArrayList<>(9999);
        LinkedList<Integer> arrTest = new LinkedList<>();
        arrTest.add(3);
        arrTest.add(8);
        arrTest.add(34);
        arrTest.add(3);
        arrTest.add(3);
        
        inputArrayAndSize(arrTest);
        
        arrTest.add(2, 87);
        
        inputArrayAndSize(arrTest);
        
        Integer a = 3;
        arrTest.remove(a);
        //arrTest.remove(3);
        
        inputArrayAndSize(arrTest);
        
        int position = arrTest.indexOf(3);
        
        System.out.println("position -> " + position);
        
        arrTest.set(2, 709);
        arrTest.get(0); // Вернет значение по индексу 0
        
        inputArray(arrTest);
    }
    
}
