package com.kkrasylnykov.l21_toolbarandnavigationdrower.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kkrasylnykov.l21_toolbarandnavigationdrower.R;

public class NavigationFragment extends Fragment {

    public final static int TYPE_CLOSE_NAVIGATION = 1001;
    public final static int TYPE_OPEN_RIGTH_NAVIGATION = 1002;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);
        rootView.findViewById(R.id.closeNavigationTextViewFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof OnMenuClickListener){
                    ((OnMenuClickListener)getActivity()).onMenuClick(TYPE_CLOSE_NAVIGATION);
                }
            }
        });
        rootView.findViewById(R.id.openRightNavigationTextViewFragment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof OnMenuClickListener){
                    ((OnMenuClickListener)getActivity()).onMenuClick(TYPE_OPEN_RIGTH_NAVIGATION);
                }
            }
        });
        return rootView;
    }

    public interface OnMenuClickListener {
        void onMenuClick(int type);
    }
}
