package com.kkrasylnykov.l21_toolbarandnavigationdrower.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kkrasylnykov.l21_toolbarandnavigationdrower.R;

public class NavigationRightFragment extends Fragment {

    public final static int TYPE_CLOSE_NAVIGATION = 1001;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation_right, container, false);
        return rootView;
    }

    public interface OnMenuClickListener {
        void onMenuClick(int type);
    }
}
