package com.kkrasylnykov.l21_toolbarandnavigationdrower.activities;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kkrasylnykov.l21_toolbarandnavigationdrower.R;
import com.kkrasylnykov.l21_toolbarandnavigationdrower.fragments.NavigationFragment;

public class MainActivity extends AppCompatActivity implements NavigationFragment.OnMenuClickListener {

    private static final int DOWNLOAD_MENU_ITEM = 1101;
    private static final int TRASH_MENU_ITEM = 1102;
    private static final int OPEN_MAIL_MENU_ITEM = 1103;
    private static final int MOVE_MENU_ITEM = 1104;
    private static final int IGNORE_MENU_ITEM = 1105;
    private static final int TO_SPAM_MENU_ITEM = 1106;

    private Toolbar toolbar;

    private DrawerLayout drawerLayout;
    private View navigationDrawer;
    private View navigationRightDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawerLayoutMainActivity);
        navigationDrawer = findViewById(R.id.navigationDrawer);
        navigationRightDrawer = findViewById(R.id.navigationEndDrawer);
//        toolbar.setTitle("Test1");
        setSupportActionBar(toolbar);

        Drawable menuIconDrawable =
                ContextCompat.getDrawable(this, R.drawable.ic_menu_black_24dp);

        menuIconDrawable.setColorFilter(
                ContextCompat.getColor(this, android.R.color.white),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(menuIconDrawable);

        Drawable menuRightIconDrawable =
                ContextCompat.getDrawable(this, R.drawable.ic_menu_right_black_24dp);

        menuRightIconDrawable.setColorFilter(
                ContextCompat.getColor(this, android.R.color.white),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setOverflowIcon(menuRightIconDrawable);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("devcpp","onClick -> ");
                drawerLayout.openDrawer(navigationDrawer);
            }
        });

        getSupportActionBar().setTitle("Test1");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case DOWNLOAD_MENU_ITEM:
                drawerLayout.openDrawer(navigationRightDrawer);
                break;
            case TRASH_MENU_ITEM:
                Toast.makeText(this, "TRASH_MENU_ITEM", Toast.LENGTH_SHORT).show();
                break;
            case OPEN_MAIL_MENU_ITEM:
                Toast.makeText(this, "OPEN_MAIL_MENU_ITEM", Toast.LENGTH_SHORT).show();
                break;
            case MOVE_MENU_ITEM:
                Toast.makeText(this, "MOVE_MENU_ITEM", Toast.LENGTH_SHORT).show();
                break;
            case IGNORE_MENU_ITEM:
                Toast.makeText(this, "IGNORE_MENU_ITEM", Toast.LENGTH_SHORT).show();
                break;
            case TO_SPAM_MENU_ITEM:
                Toast.makeText(this, "TO_SPAM_MENU_ITEM", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemDownload = menu.add(DOWNLOAD_MENU_ITEM, DOWNLOAD_MENU_ITEM,
                0, "Download");

        Drawable drawableIconDownload = ContextCompat.getDrawable(this, R.drawable.ic_download_black_24dp);
        drawableIconDownload.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemDownload.setIcon(drawableIconDownload);

        itemDownload.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemTrash = menu.add(TRASH_MENU_ITEM, TRASH_MENU_ITEM,
                1, "Trash");

        Drawable drawableIconTrash = ContextCompat.getDrawable(this, R.drawable.ic_delete_black_24dp);
        drawableIconTrash.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemTrash.setIcon(drawableIconTrash);

        itemTrash.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemOpen = menu.add(OPEN_MAIL_MENU_ITEM, OPEN_MAIL_MENU_ITEM,
                2, "Open");

        Drawable drawableIconOpen = ContextCompat.getDrawable(this, R.drawable.ic_drafts_black_24dp);
        drawableIconOpen.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemOpen.setIcon(drawableIconOpen);

        itemOpen.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);


        MenuItem itemMove = menu.add(MOVE_MENU_ITEM, MOVE_MENU_ITEM,
                3, "Move");

        Drawable drawableIconMove = ContextCompat.getDrawable(this, R.drawable.ic_move_black_24dp);
        drawableIconMove.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemMove.setIcon(drawableIconMove);

        itemMove.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemIgnore = menu.add(IGNORE_MENU_ITEM, IGNORE_MENU_ITEM,
                4, "Ignore");

        Drawable drawableIconIgnore = ContextCompat.getDrawable(this, R.drawable.ic_ignore_black_24dp);
        drawableIconIgnore.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemIgnore.setIcon(drawableIconIgnore);

        itemIgnore.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemSpam = menu.add(TO_SPAM_MENU_ITEM, TO_SPAM_MENU_ITEM,
                5, "Spam");
        itemSpam.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return true;
    }

    @Override
    public void onMenuClick(int type) {
        if (type == NavigationFragment.TYPE_CLOSE_NAVIGATION){
            drawerLayout.closeDrawer(navigationDrawer);
        } else if (type == NavigationFragment.TYPE_OPEN_RIGTH_NAVIGATION){
            drawerLayout.closeDrawer(navigationDrawer);
            drawerLayout.openDrawer(navigationRightDrawer);
        }

    }
}
