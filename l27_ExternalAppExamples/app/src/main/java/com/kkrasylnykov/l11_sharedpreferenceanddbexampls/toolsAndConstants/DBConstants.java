package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants;

public class DBConstants {
    public static final String DB_NAME = "address_db";
    public static final int DB_VERSION = 2;

    public static class DbV2 {
        public static class TableUsers{
            public static final String DB_TABLE_NAME    = "Users_v2";

            public static final String DB_FIELD_ID      = "_id";        //0
            public static final String DB_FIELD_NAME    = "_name";      //1
            public static final String DB_FIELD_SNAME   = "_sname";     //2
            public static final String DB_FIELD_ADDRES  = "_addres";    //3
            public static final String DB_FIELD_BDAY    = "_bday";      //4
        }

        public static class TablePhones{
            public static final String DB_TABLE_NAME    = "Phones_v2";

            public static final String DB_FIELD_ID      = "_id";
            public static final String DB_FIELD_USER_ID = "_user_id";
            public static final String DB_FIELD_PHONE   = "_phone";
        }
    }

    public static class DbV1 {
        public static final String DB_TABLE_NAME    = "Address";
        public static final String DB_FIELD_ID      = "_id";
        public static final String DB_FIELD_NAME    = "_name";
        public static final String DB_FIELD_SNAME   = "_sname";
        public static final String DB_FIELD_PHONE   = "_phone";
        public static final String DB_FIELD_ADDRES  = "_addres";
        public static final String DB_FIELD_BDAY    = "_bday";
    }
}
