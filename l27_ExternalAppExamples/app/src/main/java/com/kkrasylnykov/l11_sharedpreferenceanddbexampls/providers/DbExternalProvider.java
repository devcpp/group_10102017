package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

public class DbExternalProvider extends ContentProvider {
    private static final String AUTHORITIES
            ="com.kkrasylnykov.l11_sharedpreferenceanddbexampls.providers.DbExternalProvider3817845639";

    private static final String PATH_USERS = "users";
    private static final String PATH_PHONES = "phones";

    private static final int TYPE_USERS = 101;
    private static final int TYPE_PHONES = 102;

    private static UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITIES, PATH_USERS, TYPE_USERS);
        uriMatcher.addURI(AUTHORITIES, PATH_PHONES, TYPE_PHONES);
    }

    private AppDBHelper dbHelper;

    @Override
    public boolean onCreate() {
        dbHelper = new AppDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        String tableName = "";
        switch (uriMatcher.match(uri)){
            case TYPE_PHONES:
                tableName = DBConstants.DbV2.TablePhones.DB_TABLE_NAME;
                break;
            case TYPE_USERS:
                tableName = DBConstants.DbV2.TableUsers.DB_TABLE_NAME;
                break;
        }
        return tableName;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        return dbHelper.getReadableDatabase().query(getType(uri), projection, selection, selectionArgs,
                null, null, sortOrder);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        dbHelper.getWritableDatabase().insert(getType(uri), null, values);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return dbHelper.getWritableDatabase().delete(getType(uri), selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return dbHelper.getWritableDatabase().update(getType(uri), values, selection, selectionArgs);
    }
}
