package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants;

public class DBConstants {
    public static final String DB_NAME = "address_db";
    public static final int DB_VERSION = 1;

    public static final String DB_TABLE_NAME    = "Address";
    public static final String DB_FIELD_ID      = "_id";
    public static final String DB_FIELD_NAME    = "_name";
    public static final String DB_FIELD_SNAME   = "_sname";
    public static final String DB_FIELD_PHONE   = "_phone";
    public static final String DB_FIELD_ADDRES  = "_addres";
    public static final String DB_FIELD_BDAY    = "_bday";

}
