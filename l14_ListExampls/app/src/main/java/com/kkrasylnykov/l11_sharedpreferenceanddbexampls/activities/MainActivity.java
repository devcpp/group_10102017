package com.kkrasylnykov.l11_sharedpreferenceanddbexampls.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.R;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.adapters.UserInfoAdapter;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.db.AppDBHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.engines.UserEngine;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.models.UserEntity;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddbexampls.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        UserInfoAdapter.OnClickOnListListener {

//    private LinearLayout conteiner;
    private ListView listView;
    private UserInfoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSettings appSettings = new AppSettings(this);
        listView = findViewById(R.id.conteinerListViewMainActivity);
        adapter = new UserInfoAdapter();
        listView.setAdapter(adapter);
        adapter.setOnClickListListener(this);

        findViewById(R.id.addButtonMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllButtonMainActivity).setOnClickListener(this);

        if (appSettings.isFistStart()) {
            appSettings.setIsFistStart(false);
            //TODO NEED ADD CODE PROCESSING FIRST START
            Toast.makeText(this, "FIRST START", Toast.LENGTH_LONG).show();

            UserEngine userEngine = new UserEngine(this);
            for (int i=0; i<100000; i++){
                UserEntity userEntity = new UserEntity("Name_" + i, "SName_" + i,
                        "095234234" + i, "Devichia str, " + i,
                        (long)(i*100 + i*10 + i));
                userEngine.insertUser(userEntity);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateData();
    }

    private void updateData(){
        UserEngine userEngine = new UserEngine(this);
        ArrayList<UserEntity> users = userEngine.getAllUsers();
        adapter.updateData(users);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButtonMainActivity:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAllButtonMainActivity:
                AppDBHelper dbHelper = new AppDBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(DBConstants.DB_TABLE_NAME, null, null);
                db.close();

                updateData();
                break;
        }
    }

    @Override
    public void onClickItem(long id) {
        Intent editActivityIntent = new Intent(this, EditActivity.class);
        editActivityIntent.putExtra(EditActivity.KEY_USER_ID, id);
        startActivity(editActivityIntent);
    }

    @Override
    public void onClickButton(String name) {
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
    }
}
