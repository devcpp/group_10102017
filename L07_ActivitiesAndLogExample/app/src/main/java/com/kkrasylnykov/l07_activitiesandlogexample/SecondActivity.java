package com.kkrasylnykov.l07_activitiesandlogexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_STRING_NAME = "KEY_NAME";

    public static final String KEY_FOR_RESULT_STRING_BUTTON_NAME = "KEY_FOR_RESULT_STRING_BUTTON_NAME";

    private static final String TAG = "devcpp_SecondActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String strName = null;
        if (intent != null) {
//            intent.getStringExtra("name");
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                strName = bundle.getString(KEY_STRING_NAME, "defaultValue");
            }
        }

        TextView nameTextView = findViewById(R.id.nameTextViewSecondActivity);

        nameTextView.setText("Your name is " + strName + "?");

        View yesButton = findViewById(R.id.yesButtonSecondActivity);
        yesButton.setOnClickListener(this);

        findViewById(R.id.noButtonSecondActivity).setOnClickListener(this);
        findViewById(R.id.mbButtonSecondActivity).setOnClickListener(this);

        Log.d(TAG, "onCreate -> " + strName);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.yesButtonSecondActivity:
            case R.id.mbButtonSecondActivity:
                String strNameButton = ((Button) v).getText().toString();
                Intent intent = new Intent();
                intent.putExtra(KEY_FOR_RESULT_STRING_BUTTON_NAME, strNameButton);
                setResult(RESULT_OK, intent);
                break;
            case R.id.noButtonSecondActivity:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
