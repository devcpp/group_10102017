package com.kkrasylnykov.l07_activitiesandlogexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_SECOND_ACTIVITY = 123;

    private static final String TAG = "devcpp";

    private EditText mNameEditText;
    private TextView mResultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.toSecondActivityButtonMainActivity);
        button.setOnClickListener(this);

        mNameEditText = findViewById(R.id.nameEditTextMainActivity);
        mResultTextView = findViewById(R.id.resultTextViewMainActivity);
        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onClick(View v) {

        Log.d(TAG, "onClick");

        switch (v.getId()){
            case R.id.toSecondActivityButtonMainActivity:
                String strName = mNameEditText.getText().toString();

                Intent intent = new Intent(this, SecondActivity.class);
                intent.putExtra(SecondActivity.KEY_STRING_NAME, strName);
//                startActivity(intent);
                startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult -> requestCode -> " + requestCode);
        Log.d(TAG, "onActivityResult -> resultCode -> " + resultCode);
        Log.d(TAG, "onActivityResult -> data -> " + data);

        if (requestCode == REQUEST_CODE_SECOND_ACTIVITY){
            if (resultCode == RESULT_OK){
                String strButtonName = "";
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        strButtonName =
                                bundle.getString(SecondActivity.KEY_FOR_RESULT_STRING_BUTTON_NAME,
                                        "");
                    }
                }
                mResultTextView.setText("Your click -> " + strButtonName);
                mResultTextView.setVisibility(View.VISIBLE);
            } else {
                mResultTextView.setVisibility(View.GONE);
            }
        }
    }
}
